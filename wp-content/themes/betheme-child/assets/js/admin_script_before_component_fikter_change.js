jQuery(document).ready(function ($) {
    "use strict";

    var generat_filter=1;
               
    $('#configurator_id').select2();
    $('#configurator_component').select2();
    $('#config_filters').select2();
    
    //initialize filer option select2 
    function post_tag_select2(selectElementObj) {

        selectElementObj.select2({
            placeholder: 'Search filter',
            tags: true,
            multiple: true,
            minimumInputLength: 2,
            //minimumResultsForSearch: 10,
            tokenSeparators: [','],
            createTag: function (params) {
                if (params.term.trim() === '') {
                    return null;
                }
                return {
                    id: params.term.toLowerCase()+ "(New)", //How to get id here???
                    text: params.term.toLowerCase(),
                    newTag: true // add additional parameters
                }
            }
        });
    
        selectElementObj.on('select2:select', function (e) {

            if (e.params.data.newTag) {
                $.ajax( {
                    //delay: 250,
                    url: config_admin_ajax_req.ajaxurl,
                    dataType: 'json',
                    async:true,
                    type:"POST",
                    // data: function(params) {
                    //     //this.data('term', params.term);
                    //     return {
                    //         q: params.term,
                    //         action:'config_filter_tags'
                    //     };
                    // },
                    data: {
                        tag: e.params.data.text.toLowerCase(),
                        post_id: $('#curr_post_id').val(),
                        configurator_id: $('#configurator_id option:selected').val(),
                        component:$('#configurator_component option:selected').val(),
                        action:'config_filter_tags',
                        selected_post_tags:selectElementObj.val(),
                    },
                    success: function (response) {
                        let comp_tags = (response);
                        let tags_html = '';
                        if(comp_tags.component_tags) {
                            $.each(comp_tags.component_tags, function(i, item) {
                                let selected = '';
                                if(item.selected == true) {
                                    selected = 'selected';
                                }
                                tags_html += '<option value="'+item.id+'" '+selected+'>'+item.text+'</option>';
                            });
                            //$(".post_tags").html('');
                            selectElementObj.html(tags_html);
                        }
                    },
                })
            }
        });
    }

    async function get_components()
    {
        return new Promise((resolve, reject) => {
            var data = {
                'action': 'get_admin_components',
                'configurator_id': $('#configurator_id').val(),
                'current_post_id': $('#curr_post_id').val(),
                'source_selected_id' : $("#configurator_component").val()
            };
            jQuery.post(config_admin_ajax_req.ajaxurl, data)
            .then(function(res){
                resolve(res);
            })
            .catch(function(xhr, status, error) {
                reject(true);
            });
        });
    }

    async function component_tags()
    {
        return new Promise((resolve, reject) => {
            var data = {
                'action': 'get_component_tags',
                'configurator_id': $('#configurator_id').val(),
                'component' : $("#configurator_component").val(),
                'post_id': $('#curr_post_id').val()
            };
            jQuery.post(config_admin_ajax_req.ajaxurl, data)
            .then(function(res) {
                resolve(res);
            })
            .catch(function(xhr, status, error) {
                reject(true);
            });
        });
    }

    async function component_and_tags_with_relation()
    {
        return new Promise((resolve, reject) => {
            var data = {
                'action': 'get_component_and_tags_with_relation',
                'configurator_id': $('#configurator_id').val(),
                'component' : $("#configurator_component").val(),
                'post_id': $('#curr_post_id').val()
            };
            jQuery.post(config_admin_ajax_req.ajaxurl, data)
            .then(function(res) {
                resolve(res);
            })
            .catch(function(xhr, status, error) {
                reject(true);
            });
        });
    }

    function component_tag_html(count,add_new = '')
    {
        let filter_html = '';
        filter_html += '<div style="clear:both; height:5px;"></div>';
        filter_html += '<p class="" style="width:45%;float:left;">';
        filter_html += '<label style="width:20%" for="config_filter_heading">Title</label>';
        filter_html += '<input style="width:50%" type="text" class="post_title" name="post_title_tags['+count+'][title][]" value="" />';
        filter_html += '</p>';

        filter_html += '<p class="" style="width:45%;float:left;">';
        filter_html += '<label>Filters</label>';
        filter_html += '<select multiple="multiple" style="width:50%" class="post_tags" name="post_title_tags['+count+'][tags][]">';
        filter_html += '<option value=""></option>';
        filter_html += '</select>';
        filter_html += '</p>';

        if(add_new)
        {
            filter_html += '<p class="add-new-filter" style="width:10%;float:left;"><a href="javascript:void(0)">+ Add new</a></p>';
        }


        filter_html += '<div style="clear:both; height:5px;"></div>';
        //var newSelect = $(filter_html);
        $(".select-container").append(filter_html);
        if($(".component-tags-block").length > 0)
        {
            $(".component-tags-block td").append(filter_html);
        }
    }


    function component_relation_tag_html(count,add_new = '')
    {
        let filter_dest_html = '';

        filter_dest_html += '<div style="clear:both; height:5px;"></div>';
        filter_dest_html += '<p class="" style="width:50%;float: left;">';
        filter_dest_html += '<label>Components</label>';
        filter_dest_html += '<select  style="width:50%" class="component_relation_filter" name="post_relation['+count+'][component][]" id="'+count+'">';
        //filter_dest_html += $(".component_relation_filter").html();
        filter_dest_html += '</select>';
        filter_dest_html += '</p>';
        filter_dest_html += '<p class="" style="width:50%;float: left;">';
        filter_dest_html += '<label>Filters</label>';
        filter_dest_html += '<select multiple="multiple" style="width:50%" class="component_relation_tags" name="post_relation['+count+'][tag][]" id="post-relation-'+count+'">';
        filter_dest_html += '</select>';

        if(count == 0) {
            filter_dest_html += '<a class="add-filter-dest" href="javascript:void(0)">+ Add New</a>';
        }

        filter_dest_html += '</p>';

        $(".filter-dest-relation").append(filter_dest_html);
    }


    //onload: call the above function 
    $(".post_tags").each(function() {
        post_tag_select2($(this));
    });
  
    let current_post_tags_flag = 0;
    $("body").on("click",".add-new-filter", async function() {
        let filter_html = '';
        current_post_tags_flag++;
        let count = parseInt($('#current_post_filter_tags').val())+(current_post_tags_flag -1);

        component_tag_html(count);
        $('#current_post_filter_tags').val(count);

        let comp_tags = await component_tags();
        comp_tags = JSON.parse(comp_tags);
        let tags_html = '';
        if(comp_tags.component_tags) {
            $.each(comp_tags.component_tags, function(i, item) {
                tags_html += '<option value="'+item.id+'">'+item.text+'</option>';
            });
            $('.post_tags:last').html(tags_html);
        }
        post_tag_select2($('.post_tags:last'));
    });

    $('body').on('change','#configurator_id',async function() {
        let comp_resp = await get_components();
        comp_resp = JSON.parse(comp_resp);
        if (comp_resp.source_components) {
            $("#configurator_component").html(comp_resp.source_components).select2();
        } else {
            $("#configurator_component").html('').select2();
        }
        $("#configurator_component").val($("#configurator_component option:selected").val()).change();
    });

    $('body').on('change','#configurator_component',async function() {

        $('.filter-relation-block-d').hide();
        $('.show-relation-block').show();
        

        $('.select-container').html('');
        $('#component_id').val($(this).val());

        let comp_tags = await component_tags();
        comp_tags = JSON.parse(comp_tags);

        let count = parseInt($('#current_post_filter_tags').val())
        let tags_html;
        if(comp_tags.post_component_tags) {
            $.each(comp_tags.post_component_tags, function(t, tag) {

                if(t < 1) {
                    component_tag_html(t,true);
                } else {
                    component_tag_html(t);
                }

                tags_html = '';
                if(comp_tags.component_tags) {

                    $.each(comp_tags.component_tags, function(i, item) {
                        let selected_ = '';
                        if(comp_tags.post_component_tags[t].tags_id.indexOf(item.id) !== -1)
                        {
                            selected_ = 'selected';
                        }
                        tags_html += '<option value="'+item.id+'" '+selected_+'>'+item.text+'</option>';
                    });
                    $('.post_tags:last').html(tags_html);
                }
                post_tag_select2($('.post_tags:last'));
                $('.post_title:last').val(tag.title);
            });
        }
        else
        {
            component_tag_html(count - 1,true);
            tags_html = '';
            $.each(comp_tags.component_tags, function(i, item) {
                tags_html += '<option value="'+item.id+'" >'+item.text+'</option>';
            });
            $('.post_tags').html(tags_html);
            post_tag_select2($('.post_tags'));
        }
    });
    
    //Select value without triggering change event
    $('#configurator_id').val($('#configurator_id').val());
    //Select value and trigger change event
    $('#configurator_id').val($('#configurator_id').val()).change();

    function initializeSelect2WithoutTag(element){
        element.select2({
            multiple:true
        });
    }
    
    $('body').on('click','.show-relation-block', async function() {
        
        $('.filter-dest-relation').html('');

        let comp_resp = await get_components();
        comp_resp = JSON.parse(comp_resp);
        //$(".component_relation_filter:last").html(comp_resp.dest_option).select2();
        //$(".component_relation_filter:last").html(comp_resp.dest_components);
        if(Object.keys(comp_resp.relation_component_tags).length > 0)
        {
            $.each(comp_resp.relation_component_tags, function(t, item) {
                component_relation_tag_html(t);
                if(comp_resp.dest_components) {
                    let filter_html='';
                    $.each(comp_resp.dest_components, function(i, item) {
                        console.log(comp_resp.relation_component_tags[t].component);
                        console.log("comp_resp.relation_component_tags[t].component");
                        let selected_ = '';
                        console.log(comp_resp.relation_component_tags[t].component.indexOf(item.id));
                        if(comp_resp.relation_component_tags[t].component == item.id)
                        {
                            selected_ = 'selected';
                        }
                        filter_html += '<option value="'+item.id+'" '+selected_+' >'+item.text+'</option>';
                    });

                    $('.component_relation_filter:last').html(filter_html);
                }

                if(Object.keys(comp_resp.relation_component_tags[t].tags).length > 0)
                {
                    let tags_html='';
                    $.each(comp_resp.relation_component_tags[t].tags, function(tt, tag) {
                        let selected_tag = '';
                        if(Object.keys(comp_resp.relation_component_tags[t].selected_tags).length && comp_resp.relation_component_tags[t].selected_tags.indexOf(tag.id.toString()) !== -1)
                        {
                            selected_tag = 'selected';
                        }
                        tags_html += '<option value="'+tag.id+'" '+selected_tag+' >'+tag.text+'</option>';
                    });
                    $('.component_relation_tags:last').html(tags_html);
                    initializeSelect2WithoutTag($('.component_relation_tags:last'));
                }
            });       
        }
        else
        {
            component_relation_tag_html(0);
            if(comp_resp.dest_components) {
                let tags_html='';
                $.each(comp_resp.dest_components, function(i, item) {
                    if(i < 1) {
                        tags_html += '<option value="" >Select Option</option>';
                    }
                    tags_html += '<option value="'+item.id+'" >'+item.text+'</option>';
                });
                $('.component_relation_filter:last').html(tags_html);
            }
            initializeSelect2WithoutTag($('.component_relation_tags:last'));
        }
        
        $(".source_selected_component").html(comp_resp.source_selected_option);

        $('.show-relation-block').hide();
        $('.hide-relation-block').show();

        $(".component_relation_tags").each(function() {
            initializeSelect2WithoutTag($(this));
        });

        let tags_html = '';
        console.log(comp_resp.source_component_tags);
        if(comp_resp.source_component_tags) {
            $.each(comp_resp.source_component_tags, function(i, item) {
                let select_comp_tag = '';
                if(item.selected == true){
                    select_comp_tag = 'selected';
                }
                tags_html += '<option value="'+item.id+'"  '+select_comp_tag+' >'+item.text+'</option>';
            });
            $('.source_selected_tags').html(tags_html);
            initializeSelect2WithoutTag($('.source_selected_tags'));
        }
        $('.filter-relation-block-d').show();
    });

    $('body').on('click','.hide-relation-block',function() {

        $('.filter-relation-block-d').hide();
        $('.hide-relation-block').hide();
        $('.show-relation-block').show();
    });

    $('body').on('click','.add-filter-dest', async function()
    {
        let component_and_tags =  await component_and_tags_with_relation();
        component_relation_tag_html($('.component_relation_filter').length);
        component_and_tags = JSON.parse(component_and_tags);
        if(component_and_tags.relation_component) {
            let tags_html='';
            $.each(component_and_tags.relation_component, function(i, item) {
                if(i < 1) {
                    tags_html += '<option value="" >Select Option</option>';
                }
                tags_html += '<option value="'+item.id+'" >'+item.text+'</option>';
            });
            $('.component_relation_filter:last').html(tags_html);
        }
        initializeSelect2WithoutTag($('.component_relation_tags:last'));
    });


    $('body').on('change','.component_relation_filter', function()
    {
        if($(this).val() != '')
        {
            let comp_counter_id = $(this).attr("id");
            $('#post-relation-'+comp_counter_id).html('');
            var data = {
                'action': 'get_tags_by_components',
                'component' : $(this).val(),
                'post_id': $('#curr_post_id').val()
            };
            jQuery.post(config_admin_ajax_req.ajaxurl, data, function (response) {
                let component_and_tags = JSON.parse(response);
                if(component_and_tags.relation_component_tags) {
                    let tags_html='';
                    $.each(component_and_tags.relation_component_tags, function(i, item) {
                        tags_html += '<option value="'+item.id+'" >'+item.text+'</option>';
                    });
                    $('#post-relation-'+comp_counter_id).html(tags_html);
                    //$('.component_relation_filter:last').html(tags_html);
                    initializeSelect2WithoutTag($('#post-relation-'+comp_counter_id));
                }
            });
        }
    });





 
    
});