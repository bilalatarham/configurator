<?php

// register custom post type for configurator
add_action('init', 'register_configurator_post_type', 0);
function register_configurator_post_type()
{
    $labels = [
        'name' => _x('Configurators', 'Post Type General Name', 'text_domain'),
        'singular_name' => _x('Configurator', 'Post Type Singular Name', 'text_domain'),
        'menu_name' => __('Configurators', 'text_domain'),
        'parent_item_colon' => __('Parent Item:', 'text_domain'),
        'all_items' => __('All Add-ons', 'text_domain'),
        'view_item' => __('View Add-on', 'text_domain'),
        'add_new_item' => __('Add New Add-on', 'text_domain'),
        'add_new' => __('Add New Add-on', 'text_domain'),
        'edit_item' => __('Edit Add-on', 'text_domain'),
        'update_item' => __('Update Add-on', 'text_domain'),
        'search_items' => __('Search Add-on', 'text_domain'),
        'not_found' => __('Not found', 'text_domain'),
        'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
    ];

    $args = [
        'label' => __('Configurator', 'text_domain'),
        'description' => __('Create Configurator', 'text_domain'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'thumbnail', 'revisions', 'excerpt'],
        'taxonomies' => ['configurator-category', 'configurator-category'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-laptop',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post', //'page'
    ];
    register_post_type('configurator', $args);
}

// register custom texonomy for configurator post type
add_action('init', 'register_configurator_taxonomy');
function register_configurator_taxonomy()
{
    register_taxonomy(
        'configurator-category',
        'configurator',
        [
            'label' => __('Configurator Category'),
            'rewrite' => array('slug' => 'configurator-category'),
            'hierarchical' => true,
        ]
    );

    register_taxonomy(
        'configurator-manufacturer',
        'configurator',
        [
            'label' => __('Configurator Manufacturer'),
            'rewrite' => array('slug' => 'configurator-manufacturer'),
            'hierarchical' => true,
        ]
    );

// Add a taxonomy like tags
    // $labels = array(
    //     'name'                       => 'Attributes',
    //     'singular_name'              => 'Attribute',
    //     'search_items'               => 'Attributes',
    //     'popular_items'              => 'Popular Attributes',
    //     'all_items'                  => 'All Attributes',
    //     'parent_item'                => null,
    //     'parent_item_colon'          => null,
    //     'edit_item'                  => 'Edit Attribute',
    //     'update_item'                => 'Update Attribute',
    //     'add_new_item'               => 'Add New Attribute',
    //     'new_item_name'              => 'New Attribute Name',
    //     'separate_items_with_commas' => 'Separate Attributes with commas',
    //     'add_or_remove_items'        => 'Add or remove Attributes',
    //     'choose_from_most_used'      => 'Choose from most used Attributes',
    //     'not_found'                  => 'No Attributes found',
    //     'menu_name'                  => 'Attributes',
    //   );

//   $args = array(
    //     'hierarchical'          => false,
    //     'labels'                => $labels,
    //     'show_ui'               => true,
    //     'show_admin_column'     => true,
    //     'update_count_callback' => '_update_post_term_count',
    //     'query_var'             => true,
    //     'rewrite'               => array( 'slug' => 'attribute' ),
    //   );
    //   register_taxonomy('configurator_tags','configurator',$args);

}

//get custom texonomy of configurator
function get_configutor_texonomys()
{
    $texonomy = get_terms(
        [
            'taxonomy' => 'configurator-category',
            'hide_empty' => false,
            'parent' => 0,
        ]
    );

    return $texonomy;
}
//get custom chile texonomy of configurator
function get_configutor_child_texonomys($id)
{
    $child_texonomy = get_terms(
        [
            'taxonomy' => 'configurator-category',
            'hide_empty' => false,
            'parent' => $id,
        ]
    );

    return $child_texonomy;
}

//get configurator and componenets fro frot-end side
add_action('wp_ajax_get_configurat_components', 'get_configurat_components');
add_action('wp_ajax_nopriv_get_configurat_components', 'get_configurat_components');
function get_configurat_components()
{
    if ($_POST['configurator_type'] == 'component') {
        include 'pages-inner/components.php';
    } elseif ($_POST['configurator_type'] == 'config_overview') {
        include 'pages-inner/configurator-overview.php';
    }
    exit();
}

//create meta boxes for variants
add_action('add_meta_boxes', 'config_create_variant_metabox');
/* Do something with the data entered */
add_action('save_post', 'config_save_variants_data');

/* Adds a box to the main column on the Post and Page edit screens */
function config_create_variant_metabox()
{
    add_meta_box(
        'dynamic_sectionid',
        __('Filters and Component Relation', 'myplugin_textdomain'),
        'config_create_variants_fields',
        'configurator',
        'normal',
        'high'
    );
}

/* Prints the metabox content */
function config_create_variants_fields()
{
    global $post;
    // Use nonce for verification
    wp_nonce_field(plugin_basename(__FILE__), 'config_variant_nonce');
    ?>
    <div id="meta_inner">
        <?php

    //get the saved meta as an arry
    // $albums = get_post_meta( $post->ID, 'album', true );
    // $c = 0;
    // if ( count( $albums ) > 0 ) {
    //     foreach( $albums as $album ) {
    //         if ( isset( $album['title'] ) || isset( $album['track'] ) ) {
    //             printf( '<p>Heading: <input type="text" name="album[%1$s][title]" value="%2$s" /> ', $c, $album['title'], __( 'Remove Album' ) );
    //             $c = $c +1;
    //         }
    //     }
    // } ?>
        <input type="hidden" name="curr_post_id" id="curr_post_id" value="<?php echo $post->ID; ?>" />
        <p class="form-field">
            <label class="custom-post-label-block-s" for="configurator_id"><?php _e('Configurator Types'); ?></label>
            <select class="" style="width:50%" id="configurator_id" name="configurator_id">
                <?php $conf_id = get_post_meta($post->ID, 'configurator_id', true); ?>
                <?php $configurators = get_configutor_texonomys(); ?>
                <?php if (!empty($configurators)): ?>
                    <?php foreach ($configurators as $key => $configrator): ?>
                        <?php
$selected = '';
    if ($conf_id) {
        if ($configrator->term_id == $conf_id) {
            $selected = 'selected';
        }
    } elseif ($key == 0) {
        $selected = 'selected';
    }
    ?>
                        <option value="<?php echo $configrator->term_id; ?>" <?php if ($conf_id) {
        ;
    }
    ?> <?php echo $selected; ?>><?php echo $configrator->name; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </p>
        <p class="form-field">
            <label class="custom-post-label-block-s" for="configurator_component"><?php _e('Components'); ?></label>
            <select class="" style="width:50%" id="configurator_component" name="configurator_component">
                <option value=""></option>
            </select>
            <!-- <a href="javascript:void(0)" class="show-relation-block">Link Components</a> -->
        </p>


        <p class="form-field component-child-d" style="display: none;">
            <label class="custom-post-label-block-s" for="component_child"><?php _e('Child Components'); ?></label>
            <select class="" style="width:50%" id="component_child" name="component_child">
                <option value=""></option>
            </select>
        </p>

<!--
        <div class="filter-relation-block-d" style="display: none;">
                <p style="border-top:1px solid;"></p>
                <div class="" style="width: 25%;float:left">
                    <p>
                        <label><?php //_e( 'Component' ); ?></label>
                        <select style="width:50%" class="source_selected_component" name="" disabled>
                        </select>
                    </p>
                    <p>
                        <label><?php //_e( 'Filters' ); ?></label>
                        <select style="width:50%" multiple="multiple" class="source_selected_tags" name="source_selected_tags[]">
                        </select>
                    </p>
                </div>
                <div class="filter-dest-relation" style="width: 73%;float:left;border-left:1px solid">
                    <p class="" style="width:50%;float: left;">
                        <label><?php //_e( 'Destination Relation' ); ?></label>
                        <select style="width:50%" class="component_relation_filter" name="post_relation[0][component][]">
                        </select>
                    </p>
                    <p class="" style="width:50%;float:left;">
                        <label><?php //_e( 'Filter Tags' ); ?></label>
                        <select multiple="multiple" style="width:50%" class="component_relation_tags" name="post_relation[0][tag][]">
                        </select>
                        <a class="add-filter-dest" href="javascript:void(0)">Add More</a>
                    </p>
                </div>
                <br>
                <a href="javascript:void(0)" class="hide-relation-block" style="display: none;float: right;margin-bottom: 16px;margin-right: 30px;">Hide</a>
        </div> -->
        <p style="border-bottom: 1px solid #ddd;clear:both;margin: 0px;"></p>
        <?php
$post_title_tags = get_post_meta($post->ID, 'post_title_tags', true);
    $tags_counter = 1;
    if (!empty($post_title_tags)) {
        $post_title_tags = explode(',', $post_title_tags);
        $tags_counter = count($post_title_tags);
    }
    ?>
        <div style="width:100%">
            <input type="hidden" id="current_post_filter_tags" value="<?php echo $tags_counter; ?>" />
            <input type="hidden" id="component_id" name="component_id" value="<?php echo get_post_meta($post->ID, 'component_id', true); ?>" />
            <!-- <p class="" style="width:45%;float:left;">
                <label style="width:20%"><?php //_e( 'Title' ); ?></label>
                <input style="width:50%" type="text" name="post_title_tags[0][title][]" value="" />
            </p>
            <p class="" style="width:45%;float:left;">
                <label><?php //_e( 'Filter tags' ); ?></label>
                <select multiple="multiple" style="width:50%" class="post_tags" name="post_title_tags[0][tags][]">
                    <option value=""></option>
                </select>
            </p>
            <p class="add-new-filter" style="width:10%;float:left;"><a href="javascript:void(0)">+ Add new</a></p> -->
            <div style="clear: both;height:5px; margin:0px;"></div>
            <div class="select-container">
                <div style="clear:both; height:5px;"></div>
                <div class="row" style="margin:0px;">
                    <label class="custom-post-label-block-s">Filters</label>
                    <select multiple="multiple" style="width:70%" class="post_tags" name="post_title_tags[]">
                        <option value=""></option>
                    </select>
                </div>
                <div style="clear:both; height:5px;"></div>
            </div>
        </div>

        <span id="here"></span>
        <!-- <span class="button addalbum"><?php //_e('Add Album'); ?></span>
        <span class="button addtracks"><?php //_e('Add Tracks'); ?></span> -->
        <script>
            var $ =jQuery.noConflict();
            $(document).ready(function() {

                //var count = <?php //echo $c; ?>;
                $(".addtracks").click(function() {
                    count = count + 1;
                    $('#here').append('<p> Track number : <input type="text" name="album['+count+'][track]" value="" /><span class="removetrack">Remove Track</span></p>' );
                    return false;
                });

                $(".removealbum").on('click', function() {
                    $(this).parent().remove();
                });
            });
        </script>
    </div>
    <?php
}

/* When the post is saved, saves our custom data */
function config_save_variants_data($post_id)
{
    // verify if this is an auto save routine.
    // If it is our form has not been submitted, so we dont want to do anything
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if (!isset($_POST['config_variant_nonce'])) {
        return;
    }

    if (!wp_verify_nonce($_POST['config_variant_nonce'], plugin_basename(__FILE__))) {
        return;
    }


    update_post_meta($post_id, 'component_id', $_POST['component_id']);
    update_post_meta($post_id, 'source_selected_tags', wp_slash(json_encode($_POST['source_selected_tags'])));
    update_post_meta($post_id, 'post_relation', wp_slash(json_encode($_POST['post_relation'])));
    update_post_meta($post_id, 'configurator_id', $_POST['configurator_id']);
    update_post_meta($post_id, 'configurator_component', $_POST['configurator_component']);

    update_post_meta($post_id, 'component_child', $_POST['component_child']);

    if (isset($_POST['post_title_tags'][0]) && !empty($_POST['post_title_tags'][0])) {
        update_post_meta($post_id, 'post_title_tags', implode(",", $_POST['post_title_tags']));
    } else {
        update_post_meta($post_id, 'post_title_tags', '');
    }
}

//multipart form editc custom post type
function update_edit_form() {
    echo ' enctype="multipart/form-data"';
} // end update_edit_form
add_action('post_edit_form_tag', 'update_edit_form');


//create meta boxes for add-on price
add_action('add_meta_boxes', 'config_create_price_metabox');
/* Do something with the data entered */
add_action('save_post', 'config_save_price_data');

/* Adds a box to the main column on the Post and Page edit screens */
function config_create_price_metabox()
{
    add_meta_box(
        'config_prices_id',
        __('Price', 'config_textdomain'),
        'config_create_prices_fields',
        'configurator',
        'normal',
        'high'
    );
}

/* Prints the metabox content */
function config_create_prices_fields()
{
    global $post;
    // Use nonce for verification
    wp_nonce_field(plugin_basename(__FILE__), 'config_price_nonce');
    ?>
    <div id="meta_inner_price">
        <p class="form-field">
            <label><?php _e('Sku'); ?></label>
            <input type="text" name="addon_sku" value="<?php echo get_post_meta($post->ID, 'addon_sku', true); ?>" />
        </p>
        <p class="form-field">
            <label><?php _e('Actual Price'); ?></label>
            <input type="text" name="addon_actual_price" value="<?php echo get_post_meta($post->ID, 'addon_actual_price', true); ?>" />
        </p>
        <p class="form-field">
            <label><?php _e('Sale Price'); ?></label>
            <input type="text" name="addon_price" value="<?php echo get_post_meta($post->ID, 'addon_price', true); ?>" />
        </p>
        <p class="form-field">
            <?php 
                $get_attach = get_post_meta($post->ID, 'wp_custom_attachment', true);
                if(isset($get_attach['url']) && !empty($get_attach['url'])) {
                    echo "<a href=".$get_attach['url']." target='_blank'>View Attachment</a><br>";
                }
            ?>
            <label><?php _e('Upload your PDF here'); ?></label>
            <!-- show only pdf files -->
            <input type="file" id="wp_custom_attachment" name="wp_custom_attachment" value="" accept="application/pdf"/>
        </p>
    </div>
    <?php
}

/* When the post is saved, saves our custom data */
function config_save_price_data($post_id)
{
    // verify if this is an auto save routine.
    // If it is our form has not been submitted, so we dont want to do anything
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if (!isset($_POST['config_price_nonce'])) {
        return;
    }

    if (!wp_verify_nonce($_POST['config_price_nonce'], plugin_basename(__FILE__))) {
        return;
    }

    // OK, we're authenticated: we need to find and save the data
    update_post_meta($post_id, 'addon_sku', $_POST['addon_sku']);
    update_post_meta($post_id, 'addon_price', $_POST['addon_price']);
    update_post_meta($post_id, 'addon_actual_price', $_POST['addon_actual_price']);

   
   // Make sure the file array isn't empty
   if(!empty($_FILES['wp_custom_attachment']['name'])) {
         
    // Setup the array of supported file types. In this case, it's just PDF.
    $supported_types = array('application/pdf');
     
    // Get the file type of the upload
    $arr_file_type = wp_check_filetype(basename($_FILES['wp_custom_attachment']['name']));
    $uploaded_type = $arr_file_type['type'];
     
    // Check if the type is supported. If not, throw an error.
    if(in_array($uploaded_type, $supported_types)) {

        // Use the WordPress API to upload the file
        $upload = wp_upload_bits($_FILES['wp_custom_attachment']['name'], null, file_get_contents($_FILES['wp_custom_attachment']['tmp_name']));

        if(isset($upload['error']) && $upload['error'] != 0) {
            wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
        } else {
            add_post_meta($post_id, 'wp_custom_attachment', $upload);
            update_post_meta($post_id, 'wp_custom_attachment', $upload);     
        } // end if/else

    } else {
        wp_die("The file type that you've uploaded is not a PDF.");
    } // end if/else
     
} // end if



}

//get componenets for admin side side [configurator custom post type in post]
add_action('wp_ajax_get_admin_components', 'get_admin_components');
add_action('wp_ajax_nopriv_get_admin_components', 'get_admin_components');
function get_admin_components()
{
    global $wpdb;

    $id = $_POST['configurator_id'];

    $components = get_configutor_child_texonomys($id);
    $source_options = '';
    $dest_option = '';
    $source_selected_opt = '';
    $options['source_selected_tags'] = '';
    $options['dest_option'] = '';
    $options = [];

    if (!empty($components)) {
        foreach ($components as $component) {
            $selected = '';
            $source_options .= '<option value="' . $component->term_id . '" ' . $selected . ' >' . $component->name . '</option>';

            if (isset($_POST['component']) && $_POST['component'] != $component->term_id) {
                $store['id'] = $component->term_id;
                $store['text'] = $component->name;
                $store['selected'] = isset($selected) && !empty($selected) ? true : false;
                $options['dest_option'][] = $store;
            }

            if (isset($_POST['component']) && $_POST['component'] == $component->term_id) {
                $source_selected_opt .= '<option selected value="' . $component->term_id . '" ' . $selected . ' >' . $component->name . '</option>';
            }
        }

        if (isset($_POST['component']) && !empty($_POST['component'])) {
            $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
            WHERE component = " . $_POST['component'] . " ");
            $results = (array)$tags;

            $source_selected_tags = get_term_meta($_POST['component'], 'source_selected_tags', true);

            if (!empty($source_selected_tags) && ($source_selected_tags != null && $source_selected_tags != 'null')) {
                $source_selected_tags = json_decode($source_selected_tags, true);
                foreach ($source_selected_tags as $key => $sourceTag) {
                    foreach ($results as $res) {
                        $selected = 0;
                        if (in_array($res->id, $sourceTag)) {
                            $selected = 1;
                        }
                        $store['id'] = $res->id;
                        $store['text'] = $res->text;
                        $store['selected'] = $selected;
                        $options['source_selected_tags'][$key][] = $store;
                    }
                }
            } else {

                foreach ($results as $res) {
                    $selected = 0;
                    // if(!empty($source_selected_tags))
                    // {
                    //     if(in_array($res->id,$source_selected_tags))
                    //     {
                    //         $selected = 1;
                    //     }
                    // }

                    $store['id'] = $res->id;
                    $store['text'] = $res->text;
                    $store['selected'] = $selected;
                    $options['source_selected_tags'][0][] = $store;
                }
            }
        }
    }

    $options['source_components'] = $source_options;
    $options['source_selected_tags'] = $options['source_selected_tags'];
    // $options['post_relation'] = $options['post_relation'];

    $options['source_selected_option'] = $source_selected_opt;
    $options['dest_components'] = $options['dest_option'];
    $options['dest_component_tags'] = $dest_option;

    //$post_relation = get_post_meta($_POST['current_post_id'],'post_relation',true);
    $post_relation = get_term_meta($_POST['component'], 'post_relation', true);
    if (!empty($post_relation) && ($post_relation != null && $post_relation != 'null')) {
        $post_relation = json_decode($post_relation, true);
        foreach ($post_relation as $key => $val) {
            $create_comp_tags = [];
            foreach ($val as $value) {
                if (isset($value['component'][0]) && !empty($value['component'][0])) {
                    $store_post['component'] = $value['component'][0];
                    $get_tags_by_comp_id = get_tags_by_comp_id($value['component'][0]);
                    if (!empty($get_tags_by_comp_id)) {
                        $store_post['tags'] = $get_tags_by_comp_id;
                    } else {
                        $store_post['tags'] = [];
                    }

                    if (isset($value['tag'])) {
                        $store_post['selected_tags'] = $value['tag'];
                    } else {
                        $store_post['selected_tags'] = [];
                    }
                    $create_comp_tags[] = $store_post;
                }
            }
            $options['relation_component_tags'][$key] = $create_comp_tags;
        }
    } else {
        $options['relation_component_tags'] = false;
    }

    echo json_encode($options);
    exit();
}

//get componenets for admin side side [configurator custom post type in post]
add_action('wp_ajax_get_post_admin_components', 'get_post_admin_components');
add_action('wp_ajax_nopriv_get_post_admin_components', 'get_post_admin_components');
function get_post_admin_components()
{
    global $wpdb;

    $id = $_POST['configurator_id'];
    $components = get_configutor_child_texonomys($id);
    $source_options = '';

    $options = [];

    if (!empty($components)) {
        $post_component = get_post_meta($_POST['current_post_id'], 'configurator_component', true);
        foreach ($components as $component) {
            $selected = '';
            if ($post_component) {
                if ($component->term_id == $post_component) {
                    $selected = 'selected';
                }
            }
            $source_options .= '<option value="' . $component->term_id . '" ' . $selected . ' >' . $component->name . '</option>';
        }
    }
    $options['source_components'] = $source_options;
    echo json_encode($options);
    exit();
}

//get component tags by component id
function get_tags_by_comp_id($id)
{
    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    WHERE component = " . $id . " ");
    $results = (array)$tags;

    return $results;
}

//get component tags by component id
function get_tags_by_id($id)
{
    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    WHERE id = " . $id . " ");
    $results = (array)$tags;

    return $results;
}

//get componenets for admin side side [configurator custom post type in post]
add_action('wp_ajax_config_filter_tags', 'config_filter_tags');
add_action('wp_ajax_nopriv_config_filter_tags', 'config_filter_tags');
function config_filter_tags()
{
    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    WHERE configurator_id = " . $_POST['configurator_id'] . " and component = " . $_POST['component'] . " and tag = '" . $_POST['tag'] . "' ");
    if (empty($tags)) {
        $selected_tag = [];
        $saveTags =
            [
            'post_id' => $_POST['component'], //$_POST['post_id'],
             'configurator_id' => $_POST['configurator_id'],
            'component' => $_POST['component'],
            'tag' => $_POST['tag'],
        ];

        $wpdb->insert('wp_configurator_tags', $saveTags);
        $last_id = $wpdb->insert_id;
        foreach ($_POST['selected_post_tags'] as $value) {
            if (strpos($value, '(New)') !== false) {
                $selected_tag[] = $last_id;
            } else {
                $selected_tag[] = $value;
            }
        }

        $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
        WHERE configurator_id = " . $_POST['configurator_id'] . " and component = " . $_POST['component'] . " ");
        $results = (array)$tags;
        $options = [];

        foreach ($results as $res) {
            $selected = false;
            if (in_array($res->id, $selected_tag)) {
                $selected = true;
            }
            $store['id'] = $res->id;
            $store['text'] = $res->text;
            $store['selected'] = $selected;
            $options['component_tags'][] = $store;
        }
        echo json_encode($options);
    }
    exit();
}

//get componenets tags for admin side [configurator custom post type in post]
add_action('wp_ajax_get_component_tags', 'get_component_tags');
add_action('wp_ajax_nopriv_get_component_tags', 'get_component_tags');
function get_component_tags()
{
    global $wpdb;

    //
    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    WHERE component = " . $_POST['component'] . " ");

    $results = (array)$tags;

    $options = [];
    if (!empty($results)) {
        foreach ($results as $res) {
            $store['id'] = $res->id;
            $store['text'] = $res->text;
            $store['selected'] = false;
            $options['component_tags'][] = $store;
        }
    }

    $post_title_tags = get_term_meta($_POST['component'], 'post_title_tags', true);
    if (!empty($post_title_tags)) {
        $post_title_tags = json_decode($post_title_tags, true);
        foreach ($post_title_tags as $key => $post_title) {
            $store['title'] = $post_title['title'][0];
            $store['tags_id'] = $post_title['tags'];
            $store['filter_show'] = $post_title['filter_show'];
            $options['term_component_tags'][] = $store;
        }
    }

    echo json_encode($options);
    exit();
}

//get componenets tags for admin side [configurator custom post type in post]
add_action('wp_ajax_get_post_component_tags', 'get_post_component_tags');
add_action('wp_ajax_nopriv_get_post_component_tags', 'get_post_component_tags');
function get_post_component_tags()
{
    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    WHERE component = " . $_POST['component'] . " ");
    $child_tags = '';
    $child_results = '';

    $results = (array)$tags;
    $childComp = get_configutor_child_texonomys($_POST['component']);
    $options = [];
    if (isset($_POST['child_component']) && !empty($_POST['child_component']) || empty($childComp)) {
        if(!empty($_POST['child_component'])) {
            $child_tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags WHERE component = " . $_POST['child_component'] . " ");
            $child_results = (array)$child_tags;
        }
        if (!empty($results)) {
            $post_title_tags = get_post_meta($_POST['post_id'], 'post_title_tags', true);
            $con_component = get_post_meta($_POST['post_id'], 'configurator_component', true);
            $post_tags_title = get_term_meta($con_component, 'post_title_tags', true);
            $post_tags_title = json_decode($post_tags_title, true);
            if (!empty($post_title_tags)) {
                $post_title_tags = explode(',', $post_title_tags);
            }
            foreach($results as $index => $result) {
                foreach($post_tags_title as $post_title) {
                    foreach($post_title['tags'] as $pt) {
                        if($result->id == $pt) {
                           $results[$index]->title = $post_title['title'][0];
                        }
                    }
                }
            }

            foreach ($results as $res) {
                $selected = false;
                if (!empty($post_title_tags) && in_array($res->id, $post_title_tags)) {
                    $selected = true;
                }
                $store['id'] = $res->id;
                $store['text'] = $res->text;
                $store['title'] = $res->title;
                $store['selected'] = $selected;
                $options['component_tags'][] = $store;
            }
        } else {
            // sub child results 
            if (!empty($child_results)) {
        
                $post_title_tags = get_post_meta($_POST['post_id'], 'post_title_tags', true);
                $con_component = get_post_meta($_POST['post_id'], 'configurator_component', true);
                $post_tags_title = get_term_meta($con_component, 'post_title_tags', true);
                $post_tags_title = json_decode($post_tags_title, true);
                if (!empty($post_title_tags)) {
                    $post_title_tags = explode(',', $post_title_tags);
                }
                foreach($child_results as $index => $result) {
                    foreach($post_tags_title as $post_title) {
                        foreach($post_title['tags'] as $pt) {
                            if($result->id == $pt) {
                                $child_results[$index]->title = $post_title['title'][0];
                            }
                        }
                    }
                }

                foreach ($child_results as $res) {
                    $selected = false;
                    if (!empty($post_title_tags) && in_array($res->id, $post_title_tags)) {
                        $selected = true;
                    }
                    $store['id'] = $res->id;
                    $store['text'] = $res->text;
                    $store['title'] = $res->title;
                    $store['selected'] = $selected;
                    $options['component_tags'][] = $store;
                }
            }
        }
    } else {
        $component_child = get_post_meta($_POST['post_id'], 'component_child', true);
        if (!empty($childComp)) {
            $store = [];
            foreach ($childComp as $val) {
                $selected = false;
                if (!empty($component_child) && $component_child == $val->term_id) {
                    $selected = true;
                }
                $child['id'] = $val->term_id;
                $child['text'] = $val->name;
                $child['selected'] = $selected;

                $store['child_comp'][] = $child;
            }
            $options['component_tags'][] = $store;
        }
    }

    echo json_encode($options);
    exit();
}

//get componenets and tags with relationship for admin side [configurator custom post type in post]
add_action('wp_ajax_get_component_and_tags_with_relation', 'get_component_and_tags_with_relation');
add_action('wp_ajax_nopriv_get_component_and_tags_with_relation', 'get_component_and_tags_with_relation');
function get_component_and_tags_with_relation()
{
    global $wpdb;
    $components = get_configutor_child_texonomys($_POST['configurator_id']);

    $options = [];
    if (!empty($components)) {
        foreach ($components as $res) {
            if ($_POST['component'] != $res->term_id) {
                $store['id'] = $res->term_id;
                $store['text'] = $res->name;

                $options['relation_component'][] = $store;
            }
        }
    }
    echo json_encode($options);
    exit();
}

//get componenets and tags with relationship for admin side [configurator custom post type in post]
add_action('wp_ajax_get_tags_by_components', 'get_tags_by_components');
add_action('wp_ajax_nopriv_get_tags_by_components', 'get_tags_by_components');
function get_tags_by_components()
{
    global $wpdb;

    // $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    // WHERE post_id = ".$_POST['post_id']." and component = ".$_POST['component']." ");

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    WHERE component = " . $_POST['component'] . " ");
    $results = (array)$tags;

    $options = [];
    if (!empty($results)) {
        foreach ($results as $res) {
            if ($_POST['component'] != $res->id) {
                $store['id'] = $res->id;
                $store['text'] = $res->text;
                $options['relation_component_tags'][] = $store;
            }
        }
    }
    echo json_encode($options);
    exit();
}

//get componenets and tags with relationship for admin side [configurator custom post type in post]
add_action('wp_ajax_component_block', 'component_block');
add_action('wp_ajax_nopriv_component_block', 'component_block');
function component_block()
{
    include 'pages-inner/component-filter.php';
    exit();
}

// function get_config_posts_by_comp_id($id, $post = '')
// {
//     // print_r($id);
//     // print_r($post);
//     $args = [
//         'posts_per_page' => -1,
//         'post_type' => 'configurator',
//         'post_status' => 'publish',
//         //'orderby'     => 'title',
//          'order' => 'DESC',
//         //'meta_key'   => 'post_title_tags',
//          'search_component_post_title' => isset($post['s']) ? $post['s'] : '',
//         'tax_query' => [
//             'relation' => 'AND',
//             [
//                 'taxonomy' => 'configurator-category',
//                 'field' => 'term_id',
//                 'terms' => !empty($id) ? $id : $post['comp_id'],
//                 'include_children' => false,
//             ],
//         ],
//     ];

//     if (isset($post['manufactuerer']) && !empty($post['manufactuerer'])) {
//         $args['tax_query'][] =
//             [
//             'taxonomy' => 'configurator-manufacturer',
//             'field' => 'term_id',
//             'terms' => $post['manufactuerer'],
//             'include_children' => false,
//         ];
//     }

//     if (isset($post['relation']) && !empty($post['relation']) && (!isset($post['config_link_id']) || empty($post['config_link_id']))) {

//         $reltion_arr = str_replace("\\", "", $post['relation']);
//         $reltion_arr = json_decode($reltion_arr, true);
   
//         $options['relation_component_tags'] = [];
//         $prepare_search_tag = [];
        
//         if (is_array($reltion_arr) && $reltion_arr[$post['selected_configurator']] != $post['comp_id']) {
//             foreach ($reltion_arr[$post['selected_configurator']] as $rel) {

//                 if ($rel['position'] == 1) {
//                     $post_title_tag = get_post_meta($rel['addon'], 'post_title_tags', true);
                    
//                     foreach (explode(",", $post_title_tag) as $tag) {
//                         $source_selected_tags = get_term_meta($rel['comp_id'], 'source_selected_tags', true);

//                         if (!empty($source_selected_tags)) {
//                             $source_selected_tags = json_decode($source_selected_tags, true);
//                           // print_r($source_selected_tags);
                            
//                             foreach ($source_selected_tags as $s_key => $value) {

//                                 if (in_array($tag, $value)) {
                                  
//                                     $post_relation = get_term_meta($rel['comp_id'], 'post_relation', true);

//                                     if (!empty($post_relation) && ($post_relation != null && $post_relation != 'null')) {
//                                         $post_relation = json_decode($post_relation, true);

//                                         if(isset($post_relation[$s_key]))
//                                         {
//                                             foreach ($post_relation as $key => $val) {
//                                                 if($s_key == $key )
//                                                 {
//                                                     $create_comp_tags = [];
//                                                     foreach ($val as $value) {
//                                                         if (isset($value['component'][0])) {
//                                                             $store_post['component'] = $value['component'][0];
//                                                             if (isset($value['tag'])) {
//                                                                 $store_post['selected_tags'] = $value['tag'];
//                                                             } else {
//                                                                 $store_post['selected_tags'] = [];
//                                                             }
        
//                                                             $create_comp_tags[] = $store_post;
//                                                         }
//                                                     }
//                                                     $options['relation_component_tags'][$key] = $create_comp_tags;
//                                                 }
//                                             }
//                                         }
//                                     }
//                                     //print_r($options['relation_component_tags']);
//                                    // exit;
//                                     if (!empty($options['relation_component_tags'])) {
//                                         foreach ($options['relation_component_tags'] as $key => $main) {
//                                             foreach ($main as $Innerkey => $iner) {
//                                                 if ($post['comp_id'] == $iner['component']) {
//                                                     if (!empty($iner['selected_tags'])) {
//                                                         $prepare_search_tag[] = $iner['selected_tags'];
//                                                     }
//                                                 }
//                                             }
//                                         }
//                                     }
//                                 }
//                             }
//                         }
//                     }
//                 }
//                 //break;
//             }
//         }
//     }

//     if (isset($post['term_filter']) && !empty($post['term_filter'])) {
//         $count = count($post['term_filter']);
//         if ($count > 1) {
//             $args['meta_query'] = ['relation' => 'AND'];
//         }

//         foreach ($post['term_filter'] as $term_filter) {
//             $regex = '(^|,)' . $term_filter . '(,|$)';
//             $args['meta_query'][] =
//                 [
//                 'key' => 'post_title_tags',
//                 'value' => $regex,
//                 'compare' => 'REGEXP',
//             ];
//         }
//     }

//     add_filter('posts_where', 'title_filter', 10, 2);
//     $wp_query = new WP_Query($args);
//     remove_filter('posts_where', 'title_filter', 10, 2);

//     if (isset($wp_query->posts) && !empty($wp_query->posts)) {
//         $preate_search_arrr = [];
//         if (!empty($prepare_search_tag)) {
//             foreach ($prepare_search_tag as $key => $value) {
//                 foreach ($value as $val) {
//                     $preate_search_arrr[] = $val;
//                 }
//             }
//         }

//         return ['posts' => $wp_query->posts, 'searchrelation' => $preate_search_arrr];
//     } else {
//         return array();
//     }
// }




function get_config_posts_by_comp_id($id, $post = '')
{
    global $wpdb;
    $args = [
        'posts_per_page' => -1,
        'post_type' => 'configurator',
        'post_status' => 'publish',
        //'orderby'     => 'title',
         'order' => 'DESC',
        //'meta_key'   => 'post_title_tags',
         'search_component_post_title' => isset($post['s']) ? $post['s'] : '',
         // Add search manufactuerer
        'search_manufactuerer' => isset($post['manufactuerer']) ? $post['manufactuerer'] : '',
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'configurator-category',
                'field' => 'term_id',
                'terms' => !empty($id) ? $id : $post['comp_id'],
                'include_children' => false,
            ],
        ],
    ];

        // Checking manufactuerer filter with relation or without relatio, with selected filter or without 
    if (isset($post['manufactuerer']) && !empty($post['manufactuerer']) && !empty($post['term_filter'])) {
        add_filter( 'posts_clauses', 'regex_manufactuerer_query',10, 2);
    } elseif (isset($post['manufactuerer']) && !empty($post['manufactuerer']) && empty($post['term_filter'])) {
        if (isset($post['relation']) && !empty($post['relation']) && (!isset($post['config_link_id']) || empty($post['config_link_id']))) {
          
            $reltion_arr = str_replace("\\", "", $post['relation']);
            $reltion_arr = json_decode($reltion_arr, true);
            if(!empty($reltion_arr) && $reltion_arr[$post['selected_configurator']] != $post['comp_id']) {
               
                foreach ($reltion_arr[$post['selected_configurator']] as $rel) {
                 
                    if ($rel['position'] == 1) {
                        $post_title_tag = get_post_meta($rel['addon'], 'post_title_tags', true);

                        foreach (explode(",", $post_title_tag) as $tag) {
                            $source_selected_tags = get_term_meta($rel['comp_id'], 'source_selected_tags', true);
                   
                            if (!empty($source_selected_tags)) {
                                
                                 add_filter( 'posts_clauses', 'regex_manufactuerer_query',10, 2);
                                
                            } else {
                                $args['tax_query'][] =
                                [
                                    'taxonomy' => 'configurator-manufacturer',
                                    'field' => 'term_id',
                                    'terms' => $post['manufactuerer'],
                                    'include_children' => false,
                                ];
                            } 
                        }
                    }
                }
            } else {
                  
                $args['tax_query'][] =
                [
                    'taxonomy' => 'configurator-manufacturer',
                    'field' => 'term_id',
                    'terms' => $post['manufactuerer'],
                    'include_children' => false,
                ];
            }
        } else {
                  // first time
            $args['tax_query'][] =
            [
                'taxonomy' => 'configurator-manufacturer',
                'field' => 'term_id',
                'terms' => $post['manufactuerer'],
                'include_children' => false,
            ];
        }
     
    }
     

    if (isset($post['relation']) && !empty($post['relation']) && (!isset($post['config_link_id']) || empty($post['config_link_id']))) {

        $reltion_arr = str_replace("\\", "", $post['relation']);
        $reltion_arr = json_decode($reltion_arr, true);

        $options['relation_component_tags'] = [];
        $prepare_search_tag = [];

        if (is_array($reltion_arr) && $reltion_arr[$post['selected_configurator']] != $post['comp_id']) {
            foreach ($reltion_arr[$post['selected_configurator']] as $rel) {

                if ($rel['position'] == 1) {
                    $post_title_tag = get_post_meta($rel['addon'], 'post_title_tags', true);
                    foreach (explode(",", $post_title_tag) as $tag) {
                        $source_selected_tags = get_term_meta($rel['comp_id'], 'source_selected_tags', true);
                        if (!empty($source_selected_tags)) {
                            $source_selected_tags = json_decode($source_selected_tags, true);
                            $relation_filter_arr = [];
                            foreach ($source_selected_tags as $key => $value) {
                                if (in_array($tag, $value)) {
                                    $post_relation = get_term_meta($rel['comp_id'], 'post_relation', true);


                                    if (!empty($post_relation) && ($post_relation != null && $post_relation != 'null')) {
                                        $post_relation = json_decode($post_relation, true);
                                        // Filter Post relation 
                                        if (in_array($tag, $value)) {
                                            $relation_filter_arr[$key] =  $post_relation[$key];
                                        }

                                        // foreach ($post_relation as $key => $val) {
                                        foreach ($relation_filter_arr as $key => $val) {
                                            $create_comp_tags = [];
                                            foreach ($val as $value) {
                                                if (isset($value['component'][0])) {
                                                    $store_post['component'] = $value['component'][0];
                                                    if (isset($value['tag'])) {
                                                        $store_post['selected_tags'] = $value['tag'];
                                                    } else {
                                                        $store_post['selected_tags'] = [];
                                                    }

                                                    $create_comp_tags[] = $store_post;
                                                }
                                            }
                                            $options['relation_component_tags'][$key] = $create_comp_tags;
                                        }
                                    }
                                    // echo "<pre>";
                                    // print_r($options['relation_component_tags']);
                                    // echo "</pre>";
                                    if (!empty($options['relation_component_tags'])) {
                                        foreach ($options['relation_component_tags'] as $key => $main) {
                                            foreach ($main as $Innerkey => $iner) {
                                                if ($post['comp_id'] == $iner['component']) {
                                                    if (!empty($iner['selected_tags'])) {
                                                        $prepare_search_tag[] = $iner['selected_tags'];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //break;
            }
        }
    }
    $post_title_tags = get_term_meta($_POST['comp_id'], 'post_title_tags', true);

    if (!empty($post_title_tags)) {
        $post_title_tags = json_decode($post_title_tags, true);
        $filter_regex = [];
        $component_relation_filter = [];
        $termf = $post['term_filter'];
        
        
    
        foreach ($post_title_tags as $index => $filter_data) {
            $filter_regex[$filter_data['title'][0]] = $filter_data['tags'];
        }
    }
    // Filter Relation Tags Check
    if(!empty($options['relation_component_tags'])) {
        foreach($options['relation_component_tags'] as $i => $relation_com) {
            foreach($relation_com as $com_relation) {
                // check relation component id exit or not with selected component
                if($_POST['comp_id'] == $com_relation['component']){
                    foreach($com_relation['selected_tags'] as $component_rel) {
                        $component_relation_filter[] = $component_rel;
                    }
                }
            }
        }
    }

    // check term filter and relation filter condition
    if (isset($post['term_filter']) && !empty($post['term_filter']) || !empty($component_relation_filter)) {

        $args['meta_query'] = [
             'filter_regex' => $filter_regex,
             'sterm_filter' => $post['term_filter'],
             'component_relation_filter' => $component_relation_filter,
        ];
        add_filter('pre_get_posts', 'regex_filter_query');
    }
    
    add_filter('posts_where', 'title_filter', 10, 2);
    $wp_query = new WP_Query($args);
    remove_filter('posts_where', 'title_filter', 10, 2);
    // Remove Filters
    remove_filter('posts_where', 'regex_filter_query');
    remove_filter( 'posts_clauses', 'regex_manufactuerer_query',10, 2);


    if (isset($wp_query->posts) && !empty($wp_query->posts)) {
        $preate_search_arrr = [];
        if (!empty($prepare_search_tag)) {
            foreach ($prepare_search_tag as $key => $value) {
                foreach ($value as $val) {
                    $preate_search_arrr[] = $val;
                }
            }
        }

        return ['posts' => $wp_query->posts, 'searchrelation' => $preate_search_arrr];
    } else {
        return array();
    }
}

function regex_filter_query( $wp_query ) {
   
    $wp_query->set( 'meta_key', 'post_title_tags' );    
    add_filter( 'posts_where', 'filter_meta_query',10, 2);
}
function filter_meta_query( $where = '', &$wp_query ) {

    global $wpdb;

    $where = '';
    if ($wp_query->query['meta_query']) {
        $filter = $wp_query->query['meta_query']['filter_regex'];
        $sql_query = '';
        $sterm_filter = $wp_query->query['meta_query']['sterm_filter'];
        $component_relation_filter = $wp_query->query['meta_query']['component_relation_filter'];
    

        $search_term = $wp_query->get('search_component_post_title');
        $search_query = '';
        
        $search_bracket = '';
         if (isset($search_term) && !empty($search_term)) {
           
             $search_query .= ' ) AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql(like_escape($search_term)) . '%\'';
        }
        
        $f=0;
        $orc = 0;
        $store_unselected =[];
        $group_not_selected =[];
        
        foreach($filter as $checked) {
            $flag = 0;
            $iterate = 0;

            $group_selected = [];
            
           
            foreach($checked as $index => $c) {

                $or = 'OR';
                if ($index == end($checked)){
                    $or = '';
                }
                // Check select filter is not empty and relation filter is empty then enter
                if(!empty($sterm_filter) && empty($component_relation_filter)) {
                    if(in_array($c,$sterm_filter)) {

                        //search 
                        if(isset($search_term) && !empty($search_term)) {
                            if($orc < 1) {
                                $search_bracket = '(';
                            } else {
                                $search_bracket = '';
                            }
                        }


                        $orc++;
                        if($iterate < 1)
                        {
                            $sql_query .= " AND $search_bracket ( ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value REGEXP '(^|,)$c(,|$)' )"; 
                        }
                        else
                        {
                            $sql_query .= "OR ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value REGEXP '(^|,)$c(,|$)' ) ";
                        }
                        $iterate++;

                        $flag = 1;
                        $f++;
                        $group_selected[] = $c;
                    }
                }
                 // Check relation filter is not empty and select filter is empty then enter
                elseif(!empty($component_relation_filter) && empty($sterm_filter)) {
                    if(in_array($c,$component_relation_filter)) {
               
                        if(isset($search_term) && !empty($search_term)) {
                            if($orc < 1) {
                                $search_bracket = '(';
                            } else {
                                $search_bracket = '';
                            }
                        }

                        $orc++;
                        if($iterate < 1)
                        {
                            $sql_query .= " AND $search_bracket ( ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value REGEXP '(^|,)$c(,|$)' )"; 
                        }
                        else
                        {
                            $sql_query .= "OR ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value REGEXP '(^|,)$c(,|$)' ) ";
                        }
                        $iterate++;

                        $flag = 1;
                        $f++;
                        $group_selected[] = $c;
                    }
                      // Check both condition at once if both not empty
                } elseif(!empty($component_relation_filter) && !empty($sterm_filter)) {
                    // Combine both arrays and then filter it in in_array
                        if(in_array($c,array_merge($sterm_filter,$component_relation_filter))) {
                         
                                if(isset($search_term) && !empty($search_term)) {
                                    if($orc < 1) {
                                        $search_bracket = '(';
                                    } else {
                                        $search_bracket = '';
                                    }
                                }


                                $orc++;
                                if($iterate < 1)
                                {
                                    $sql_query .= " AND $search_bracket ( ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value REGEXP '(^|,)$c(,|$)' )"; 
                                }
                                else
                                {
                                    $sql_query .= "OR ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value REGEXP '(^|,)$c(,|$)' ) ";
                                }
                                $iterate++;

                                $flag = 1;
                                $f++;
                                $group_selected[] = $c;
                            
                        }
                }

            }

            if($flag) {
                
                if(!empty($group_selected))
                {
                    $arra_diff = array_diff($checked,$group_selected );
                    foreach($arra_diff as $a_diff){
                        $group_not_selected[] = $a_diff;
                    }
                }
                $sql_query .= ' ) ' ;
            }
            else
            {
                $store_unselected[] = $checked;
            }
        }

        if(!empty($group_not_selected))
        {
            $i = 0;
            foreach ($group_not_selected as $key => $value) {
                $sql_query .= " and ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value NOT REGEXP '(^|,)$value(,|$)' ) ";
            }
        }
       

        // if(!empty($store_unselected))
        // {
        //     $i = 0;
        //     //$sql_query .= " AND( ";
        //     foreach ($store_unselected as $key => $value) {
        //         for($r = 0; $r<count($value); $r++ ) {
                    
        //             $or = '';
        //             //if($orc > 0 && $i > 0)
        //             if($orc > 0 )
        //             {
        //                 $or = " OR ";
        //             }
        //             $sql_query .= " ".$or." ( $wpdb->postmeta.meta_key = 'post_title_tags' AND $wpdb->postmeta.meta_value REGEXP '(^|,)$value[$r](,|$)' ";
                    
        //             // and or not selected group
        //             foreach ($group_not_selected as $key => $value) {
        //                 $sql_query .= " AND $wpdb->postmeta.meta_value NOT REGEXP '(^|,)$value(,|$)'  ";
        //             }

                    
        //             $sql_query .=" ) ";


        //             //$i++;
        //         }
        //         $i++;
                
        //     }
        //    // $sql_query .= " ) ";
        // }


        

       

        $where .= $sql_query;
    }
    //return $where;
    
    return $where.''.$search_query;
}

function title_filter($where, &$wp_query)
{

    global $wpdb;
    
    if(empty($wp_query->query['meta_query']['filter_regex'])) {
        if ($search_term = $wp_query->get('search_component_post_title')) {
            $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql(like_escape($search_term)) . '%\'';
        }
    }

    return $where;
    
}
// Add Filter for manufactuerer search
function regex_manufactuerer_query($clauses, $wp_query) {

      global $wpdb;
      $manufactuerer = $wp_query->get('search_manufactuerer');
      
      if(!empty($manufactuerer)) {
        $clauses['join'] .= " INNER JOIN $wpdb->term_taxonomy  AS tt ON $wpdb->term_relationships.term_taxonomy_id = tt.term_taxonomy_id 
        INNER JOIN $wpdb->terms AS t ON tt.term_id = t.term_id
        ";
        $clauses['where'] .= " AND (( tt.taxonomy IN ('configurator-manufacturer')  AND t.term_id in ($manufactuerer)))";
    }
    
        return $clauses;

}
function group_config_tags_manufacturer($id, $post = '')
{
    $get_posts = get_config_posts_by_comp_id($id, $post);

    $return = [];
    if (isset($get_posts['posts'])) {
        foreach ($get_posts as $get_post) {
            foreach($get_post as $p) {
                $term = wp_get_post_terms($p->ID, 'configurator-manufacturer');
                if (!empty($term)) {
                    foreach ($term as $mft) {
                        $return['manufacturer'][$mft->term_id] = ['id' => $mft->term_id, 'text' => $mft->name];
                    }
                }
            }
        }
    }

    $return['searchrelation'] = [];
    if (isset($get_posts['searchrelation']) && !empty($get_posts['searchrelation'])) {
        $return['searchrelation'] = $get_posts['searchrelation'];
    }

    $post_title_tags = get_term_meta($id, 'post_title_tags', true);

    if (!empty($post_title_tags)) {
        $post_title_tags = json_decode($post_title_tags, true);
        foreach ($post_title_tags as $filter_data) {
            if (!empty($filter_data['tags'])) {
                $filter['title'] = $filter_data['title'][0];
                $filter['show_filter'] = $filter_data['filter_show'];
                $filter['tag'] = get_tags_by_in_ids($filter_data['tags']);
                $return['tags'][] = $filter;
            }
        }
    }
    // echo "<pre>";
    // print_r($return);
    // exit;
    return $return;
}

function get_tags_by_in_ids($ids)
{
    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags
    WHERE id IN(" . implode(',', $ids) . ") ");
    $results = (array)$tags;

    return $results;
}

add_action('wp_ajax_search_component_posts', 'search_component_posts');
add_action('wp_ajax_nopriv_search_component_posts', 'search_component_posts');
function search_component_posts()
{
    include 'pages-inner/component-filter-posts.php';
    exit();
}

function get_term_by_id($id)
{
    return get_term($id);
}

add_action('wp_ajax_get_child_component', 'get_child_component');
add_action('wp_ajax_nopriv_get_child_component', 'get_child_component');
function get_child_component()
{
    if ($_POST['type'] == 'child_component') {
        include 'pages-inner/component-children.php';
    } elseif ($_POST['type'] == 'config_child_overview') {
        include 'pages-inner/configurator-child-overview.php';
    }

    exit();
}
// old function
// function validate_component($post)
// {
//     global $wpdb;

//     if (!empty($post['configurator_id']) && !empty($post['component']) && !empty($post['relation'])) {

//         $reltion_arr = str_replace("\\", "", $post['relation']);
//         $reltion_arr = json_decode($reltion_arr, true);

//         $component_validate = [];

//         if (is_array($reltion_arr) && isset($reltion_arr[$post['configurator_id']]) && !empty($reltion_arr[$post['configurator_id']])) {
//             foreach ($reltion_arr as $key => $value) {
//                 foreach ($value as $rel) {
//                     $post_title_tag = get_post_meta($rel['addon'], 'post_title_tags', true);
//                     if (!empty($post_title_tag)) {
//                         $post_title_tag = explode(",", $post_title_tag);
//                         foreach ($post_title_tag as $val) {
//                             $comp = $wpdb->get_results("SELECT * FROM wp_configurator_validate_condition
//                             WHERE configurator_id = " . $_POST['configurator_id'] . " and component = " . $_POST['component'] . " and find_in_set(" . $val . ",filter) ");
//                             $results = (array)$comp;
//                             if (isset($results[0]->id)) {
//                                 $component_validate[] = $results;
//                                 break;
//                             }
//                         }
//                     }
//                     break;
//                 }
//                 break;
//             }
//         }
//         return $component_validate;
//     }
// }
// new function 
function validate_component($post)
{
    
    global $wpdb;

    if (!empty($post['configurator_id']) && !empty($post['component']) && !empty($post['relation'])) {

        $reltion_arr = str_replace("\\", "", $post['relation']);
        $reltion_arr = json_decode($reltion_arr, true);

        $component_validate = [];
        $position_one_component = '';
        if (is_array($reltion_arr) && isset($reltion_arr[$post['configurator_id']]) && !empty($reltion_arr[$post['configurator_id']])) {

            foreach ($reltion_arr as $key => $value) {
              

              
                foreach ($value as $rel) {
                    // validate based on position 1 component
                    if ($rel['position'] == 1) {
                        $post_title_tag = get_post_meta($rel['addon'], 'post_title_tags', true);
                        if (!empty($post_title_tag)) {
                            $post_title_tag = explode(",", $post_title_tag);
                            foreach ($post_title_tag as $val) {
                              
                                $comp = $wpdb->get_results("SELECT * FROM wp_configurator_validate_condition
                                WHERE configurator_id = " . $_POST['configurator_id'] . " and component = " . $rel['comp_id'] . " and find_in_set(" . $val . ",filter) ");
                                $results = (array)$comp;
                                if (isset($results[0]->id)) {
                                    $component_validate[] = $results;
                                    break;
                                }
                            }
                        }
                    }  
                }
                break;
               
            }
        }
        return $component_validate;
    }
}
//front-end
// get pre-selected components and addons kits
function pre_selected_kits($prod_id, $type = '')
{
    $return = [];
    $configurator = get_post_meta($prod_id, 'prefix_kit_configurator', true);
   
    if ($configurator && $type == '') {
        $return['configurator'] = $configurator;
        $component = get_post_meta($prod_id, 'prefix_kit_component', true);
        if (!empty($component)) {
            $selected_comp = json_decode($component, true);
            if (!empty($selected_comp)) {
                $return['component'] = $selected_comp;
            }
        }

        $child_component = get_post_meta($prod_id, 'prefix_kit_child_component', true);
        if (!empty($child_component)) {
            $selected_c_comp = json_decode($child_component, true);
            if (!empty($selected_c_comp)) {
                $return['child_component'] = $selected_c_comp;
            }
        }

        $addon = get_post_meta($prod_id, 'prefix_kit_addon', true);
        if (!empty($addon)) {
            $addon = json_decode($addon, true);
            $return['addon'] = $addon;
        }
    } elseif ($type == 'lock') {
        $lock = get_post_meta($prod_id, 'prefix_kit_lock_component', true);
        $lock = json_decode($lock, true);
        if (!empty($lock)) {
            $return['lock'] = $lock;
        }
    } elseif ($type == 'addon') {
        $addon = get_post_meta($prod_id, 'prefix_kit_addon', true);
        $addon = json_decode($addon, true);
        if (!empty($addon)) {
            $return['addon'] = $addon;
        }
    }
    return $return;
}

//front-end
add_action('wp_head', 'pre_select_component_addonds');
function pre_select_component_addonds()
{
    $product_object = [];
    $kits_object = 'var kits_pre_selected = {};';
    if (isset($_GET['config_link_id']) && !empty($_GET['config_link_id'])) {
        $pre_selected = pre_selected_kits($_GET['config_link_id']);

        //print_r($pre_selected);
        if (isset($pre_selected['configurator']) && !empty($pre_selected['configurator'])
            && isset($pre_selected['component']) && !empty($pre_selected['component'])
            && isset($pre_selected['addon']) && !empty($pre_selected['addon'])) {

           
            foreach ($pre_selected['component'] as $comp) {

                if(isset($pre_selected['child_component'][$comp]) && !empty($pre_selected['child_component'][$comp]) && isset($pre_selected['addon'][$pre_selected['child_component'][$comp]]) && !empty($pre_selected['addon'][$pre_selected['child_component'][$comp]])   )
                {
                    // $product_object[$pre_selected['configurator']][$comp][$pre_selected['child_component'][$comp]] =
                    $product_object[$pre_selected['configurator']][$pre_selected['child_component'][$comp]] =
                    [
                    'comp_id' => $comp,
                    'addon' => $pre_selected['addon'][$pre_selected['child_component'][$comp]][0],
                    'price' => get_post_meta($pre_selected['addon'][$pre_selected['child_component'][$comp]][0], 'addon_price', true),
                    'position' => '',
                    'qty' => 1,
                ];
                }
                else if (isset($pre_selected['addon'][$comp][0]) && !empty($pre_selected['addon'][$comp][0])) 
                
                {
                    $product_object[$pre_selected['configurator']][$comp] =
                        [
                        'comp_id' => $comp,
                        'addon' => $pre_selected['addon'][$comp][0],
                        'price' => get_post_meta($pre_selected['addon'][$comp][0], 'addon_price', true),
                        'position' => '',
                        'qty' => 1,
                    ];
                }
            }


        }
        if (!empty($product_object)) {
            $kits_object = 'var kits_pre_selected = {"kits":' . json_encode($product_object) . ', "config_link_id":"' . $_GET['config_link_id'] . '"};';
        }
    }

    $config_oject = get_config_default_product();

    $configurator_object = 'var config_pre_selected = {"configurator":' . json_encode($config_oject) . '};';

    echo "<script type='text/javascript'>\n";
    echo "/* <![CDATA[ */\n";
    echo $kits_object;
    echo "\n";
    echo $configurator_object;
    echo "\n/* ]]> */\n";
    echo "</script>\n";
}

//configrator items addd to cart

add_action('wp_ajax_configurator_cart', 'configurator_cart');
add_action('wp_ajax_nopriv_configurator_cart', 'configurator_cart');
function configurator_cart()
{
    global $woocommerce;
    $post = $_POST;
    $qty = 1;
    $woocommerce->cart->add_to_cart($post['product_id'], $qty);
}

// Logic to Save products custom fields values into the cart item data
//This captures additional posted information (all sent in one array)
add_filter('woocommerce_add_cart_item_data', 'configurator_add_item_data', 1, 10);
function configurator_add_item_data($cart_item_data, $product_id)
{
    if (!isset($_POST['configurator_detail']) && isset($_POST['product_id'])) {

        $new_value = array();

        $pre_selected = pre_selected_kits($_POST['product_id']);
        if (isset($pre_selected['configurator']) && !empty($pre_selected['configurator'])
            && isset($pre_selected['component']) && !empty($pre_selected['component'])
            && isset($pre_selected['addon']) && !empty($pre_selected['addon'])) {
            foreach ($pre_selected['component'] as $comp) {
                if (isset($pre_selected['addon'][$comp][0]) && !empty($pre_selected['addon'][$comp][0])) {
                    $new_value['config_component_object'][$comp] =
                    [
                        'component_name' => get_term_by_id($comp)->name,
                        'addon_name' => get_the_title($pre_selected['addon'][$comp][0]),
                        'addon_price' => get_post_meta($pre_selected['addon'][$comp][0], 'addon_price', true),
                        'qty' => 1,
                        'total_price' => get_post_meta($pre_selected['addon'][$comp][0], 'addon_price', true),
                    ];
                }
            }
        }
    } else {

        $reltion_arr = str_replace("\\", "", $_POST['configurator_detail']);
        $config_cart = json_decode($reltion_arr, true);
        // $config_id = array_key_first($config_cart);
        // current config id
        $config_id = $_POST['selected_configurator'];

        if (isset($config_cart[$config_id])) {
            $new_value = array();
            foreach ($config_cart[$config_id] as $key => $value) {
                $price = get_post_meta($value['addon'], 'addon_price', true);
                $total_price = 0;
                if ($price > 0) {
                    $total_price = $price * $value['qty'];
                }
                $new_value['config_component_object'][$key] = [
                    'component_name' => get_term_by_id($key)->name,
                    'addon_name' => get_the_title($value['addon']),
                    'addon_price' => $price,
                    'qty' => $value['qty'],
                    'total_price' => $total_price,
                ];
            }
            $new_value['config_object'] = $config_cart;
        }
    }

    if (empty($cart_item_data)) {
        return $new_value;
    } else {
        return array_merge($cart_item_data, $new_value);
    }
}

//This captures the information from the previous function and attaches it to the item.
add_filter('woocommerce_get_cart_item_from_session', 'configurator_get_cart_items_from_session', 1, 3);
function configurator_get_cart_items_from_session($item, $values, $key)
{
    if (array_key_exists('config_component_object', $values)) {
        $item['config_component_object'] = $values['config_component_object'];
    }
    return $item;
}

//This displays extra information on cart & checkout from within the added info that was attached to the item.
add_filter('woocommerce_cart_item_name', 'configurator_add_usr_custom_session', 1, 3);
function configurator_add_usr_custom_session($product_name, $values, $cart_item_key)
{
    $html = '';
    $html .= '<div>';
    if(isset($values['config_component_object']))
    {
        foreach ($values['config_component_object'] as $comp_obj) {

            $html .= '<div class="component-block-s">';
            $html .= '<span><strong>Component: </strong> ' . $comp_obj['component_name'] . ' </span>';
            $html .= '<span><strong>addon: </strong> ' . $comp_obj['addon_name'] . '</span>';
            $html .= '<span><strong>Price: </strong> ' . $comp_obj['qty'] . ' X ' . $comp_obj['addon_price'] . ' = ' . $comp_obj['total_price'] . ' </span>';
            $html .= '</div>';
        }
        $html .= '</div>';
    }
    $return_string = $product_name . "<br />" . $html;

    return $return_string;
}

//This adds the information as meta data so that it can be seen as part of the order (to hide any meta data from the customer just start it with an underscore)

add_action('woocommerce_add_order_item_meta', 'wdm_add_values_to_order_item_meta', 1, 2);
function wdm_add_values_to_order_item_meta($item_id, $values)
{
    global $woocommerce, $wpdb;

    $html = '';
    $html .= '<div>';
    foreach ($values['config_component_object'] as $comp_obj) {

        $html .= '<div class="component-block-s">';
        $html .= '<span><strong>Component</strong> : ' . $comp_obj['component_name'] . ' </span>';
        $html .= '<span><strong>addon:</strong> : ' . $comp_obj['addon_name'] . '</span>';
        $html .= '<span><strong>Price: </strong> ' . $comp_obj['qty'] . ' X ' . $comp_obj['addon_price'] . ' = ' . $comp_obj['total_price'] . ' </span>';
        $html .= '</div>';
    }
    $html .= '</div>';

    wc_add_order_item_meta($item_id, 'order_item_details', $html);

    // wc_add_order_item_meta($item_id,'item_details',$values['_custom_options']['description']);
    // wc_add_order_item_meta($item_id,'customer_image',$values['_custom_options']['another_example_field']);
    // wc_add_order_item_meta($item_id,'_hidden_field',$values['_custom_options']['hidden_info']);
}

//If you want to override the price you can use information saved against the product to do so
add_action('woocommerce_before_calculate_totals', 'update_custom_price', 1, 1);
function update_custom_price($cart_object)
{
    foreach ($cart_object->cart_contents as $cart_item_key => $value) {

        if (isset($value['config_component_object'])) {

            $custom_price = 0;
            foreach ($value['config_component_object'] as $cal_price) {
                $custom_price += (float)$cal_price['total_price'];
            }
            if ($custom_price > 0) {
                $value['data']->set_price($custom_price);
            }
        }
    }
}

//front-end
//get the defined product for configurators
function get_config_default_product()
{
    global $wpdb;
    $conditions = $wpdb->get_results("SELECT * FROM wp_configurator_settings");

    return $conditions;
}

function surveillan_cart_redirect_url() {
    return esc_url('/');
}
add_filter('woocommerce_return_to_shop_redirect', 'surveillan_cart_redirect_url' );

add_action('restrict_manage_posts','addons_category_fitler');
// Show Category filter at configurator post type page
function addons_category_fitler() {
    $screen = get_current_screen();
    if ( $screen->post_type == 'configurator'  ) { 
        $selected_id = @$_GET['configurator_category'];
      $args = [
            'show_option_all'    => 'Configurator Category',
            'orderby'            => 'ID', 
            'order'              => 'ASC',
            'hide_empty'         => 0, 
            'hierarchical'       => 1, 
            'selected'           => $selected_id,
            'taxonomy'           => 'configurator-category',
            'hide_if_empty'      => false,
            'class'              => 'configurator-category-class',
            'name'               => 'configurator_category'


        ];
        wp_dropdown_categories( $args );

    
        
    }
}
add_action('restrict_manage_posts','addons_price_fitler');
// Show Price filter at configurator post type page
function addons_price_fitler() {
    $screen = get_current_screen();
    if ($screen->post_type == 'configurator') { ?>
       <label for="start_date">Price:</label>
        <input type="text"  name="configurator_price" value="<?php echo @$_GET['configurator_price']; ?>">
        <?php
    }
}
// Show Custom Columns add post type configurator

function add_configurator_columns ( $columns ) {
    unset($columns['date']);
    return array_merge ( $columns, [
       'configurator_category' => __ ('Category'),
       'configurator_price' => __ ('Price'),
       'date' => __('Date')
    ]);

 }

add_filter ( 'manage_configurator_posts_columns', 'add_configurator_columns' );

// Add the data to the custom columns for the configurator post type:
add_action( 'manage_configurator_posts_custom_column' , 'custom_configurator_column', 10, 2 );
function custom_configurator_column( $column, $post_id ) {
    switch ( $column ) {
        case 'configurator_category' :
        global $wpdb;
        // Show Category child level
        $component = get_post_meta($post_id);
        if(!empty($component['component_id'][0])) {
            $id = $component['component_id'][0];
        }
        if(isset($component['component_child'][0]) && !empty($component['component_child'][0])) {
            $id = $component['component_child'][0];
        }
        if(!empty($id)) {
            $term = $wpdb->get_row("SELECT name FROM $wpdb->terms where term_id = $id");
            echo $term->name;
        }
        break;
        case 'configurator_price' :
            $addon_price = get_post_meta($post_id, 'addon_price', true);
            echo $addon_price;
            break;
    }
}

add_filter( 'parse_query' , 'configurator_price_filter' );
// price filter for configurator post type
function configurator_price_filter($query) {
    global $pagenow;
    $post_type = 'configurator'; 
    $q_vars    = &$query->query_vars;
    $q_vars['meta_query'] = array();
    if ($pagenow == 'edit.php' && 
        isset($q_vars['post_type']) && 
        $q_vars['post_type'] == $post_type && 
        @$_GET['configurator_price']) {
        $q_vars['meta_query'][] = array(
            'key' => 'addon_price',
            'value' => @$_GET['configurator_price'],
            'compare' => '='
        );
    }
}
add_filter( 'parse_query' , 'configurator_category_filter' );

// category filter for configurator post type
function configurator_category_filter($query) {

    global $pagenow, $wpdb;
    $post_type = 'configurator'; 
    $q_vars    = &$query->query_vars;

    $q_vars['meta_query'] = array();
    if ($pagenow == 'edit.php' && 
        isset($q_vars['post_type']) && 
        $q_vars['post_type'] == $post_type && 
        @$_GET['configurator_category']) {
        $q_vars['meta_query'][] = array(
            'key' => 'configurator_component',
            'value' => @$_GET['configurator_category'],
            'compare' => '='
        );
    }
}
// Update cart counter request ajax
add_action('wp_ajax_cart_count_retriever', 'cart_count_retriever');
add_action('wp_ajax_nopriv_cart_count_retriever', 'cart_count_retriever');
function cart_count_retriever() {
    global $wpdb;
    echo WC()->cart->get_cart_contents_count()+1;
    wp_die();
}

// get print data sheet and save in session
function print_data_sheet_ajax() {
    $reltion_arr = str_replace("\\", "", $_POST['selected_addons']);
    $reltion_arr = json_decode($reltion_arr, true);
    $_SESSION['datasheet_session'] = $reltion_arr;
    $redirect = get_site_url().'/datasheet-overview/';
    echo $redirect;
    exit();
}

add_action('wp_ajax_print_data_sheet_ajax', 'print_data_sheet_ajax');
add_action('wp_ajax_nopriv_print_data_sheet_ajax', 'print_data_sheet_ajax');




// data sheet shortcode
function data_sheet_shortcode(){ ?>
<?php 
ob_start();  
    $data_sheet_data = @$_SESSION['datasheet_session']; 
    
    ?>
    <?php
        $post_ids = [];
        foreach($data_sheet_data as $sheet_data) {
            foreach($sheet_data as $data) {
                $post_ids[] = $data['addon'];
            }
        }
    ?>

    <div class="content-wrapper">
        <div class="container-fluid">
          
            <div class="row">
                <div class="col-sm-4 col-12">
                    <div class="img-col mb-2">
                        <a class="datasheet-brand" href="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/logo-1.png" alt="Brand Logo" >
                        </a>
                    </div>
                </div>
                <div class="col-sm-8 col-12">
                    <div class="row">
                        <div class="col-sm-6  pb-2">
                        
                            <div class="m-list-item ">
                                <div class="icon"><img class="scale-with-grid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/experience.svg"></div>
                                <div class="content">20 Years <span>Experience</span></div>
                            </div>
                        
                        </div>
                        <div class="col-sm-6  pb-2">
                        
                            <div class="m-list-item ">
                                <div class="icon"><img class="scale-with-grid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/positive-vote.svg"></div>
                                <div class="content">Made In<span>Germany</span></div>
                            </div>
                        
                        </div>
                        <div class="col-sm-6 pb-2">
                        
                            <div class="m-list-item ">  
                                <div class="icon"><img class="scale-with-grid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/delivery.svg"></div>
                                <div class="content"> Express <span>Assembly</span></div>
                            </div>
                        
                        </div>
                        <div class="col-sm-6 pb-2">
                        
                            <div class="m-list-item ">  
                                <div class="icon"><img class="scale-with-grid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/medal.svg"></div>
                                <div class="content"> 4 Years <span>guarantee</span></div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>

            <section class="facebook-section">
                <div class="row mt-5">
                    <div class="col-sm-6">
                        <a class="facebook-link" href="#">
                            <div class="facebook">
                                <img class="scale-with-grid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/facebook.svg">
                                <span>facebook.com/example</span>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6">
                        <a class="facebook-link" href="#">
                            <div class="email float-right">
                                <img class="scale-with-grid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/ekomi.png">
                                <!-- <span>Email</span> -->
                            </div>
                        </a>
                    </div>
                </div>
            </section>

            <section class="custom-pc-section">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dark-bg-heading">
                          <h3>Custom PC configuration</h3>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="crox-img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/data-sheet/images/no-sign-bg.png">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="progress-section">
                            <h3 class="progress-bar-title">ARTICLE NUMBER: indipc</h3>
                            <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="label-progressbar">Office</span>
                            <div class="progress">
                              <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="label-progressbar">Multimedia</span>
                            <div class="progress">
                              <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="label-progressbar">Gaming</span>

                            <div class="progress">
                              <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="label-progressbar">Workstation/CAD</span>

                            <div class="progress">
                              <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                             </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="component-overview mt-5">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="dark-bg-heading">
                          <h3>Component Overview</h3>
                        </div>
                    </div>
                    <?php if(!empty($data_sheet_data)) { ?>
                    <div class="col-sm-12">
                        <div class="components-block">

                            <?php 
                            $index = 0;

                            $query = new WP_Query(
                            [
                            'post_type' => 'configurator',
                            'post__in'  => $post_ids,
                            ]);  
                            if($query->have_posts()) {
                            while ($query->have_posts()) : $query->the_post(); 
                            
                            $get_attach = get_post_meta(get_the_ID(), 'wp_custom_attachment', true);
                            if(isset($get_attach['url']) && !empty($get_attach['url'])) { ?>
                                        <div class="component clearfix">
                                            <div class="image">
                                                <?php 
                                            $url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                                            if (!empty($url)) {
                                            $image = $url;
                                            } else {
                                            $image = get_stylesheet_directory_uri()."/assets/images/no-image.png";
                                            } ?>

                                                <img src="<?php echo $image; ?>" alt="Cardreader intern">
                                            </div>
                                            <div class="title">
                                                <h4>
                                                     <a href="javascript:void(0)">             
                                                        <span class="optionName"><?php the_title(); ?></span>
                                                    </a>
                                                    <a data-toggle="collapse" data-target="#component_content_<?php echo $index; ?>">▼</a>
                                                    
                                                        <a class="button-style view-button-s" href="<?php echo $get_attach['url']; ?>" target='_blank'>             
                                                            <span class="optionName">View</span>
                                                        </a>
                                                        <a class="button-style download-button-s" href="<?php echo $get_attach['url']; ?>" download>             
                                                            <span class="optionName">Download</span>
                                                        </a>
                                               
                                                
                                               
                                                    
                                                </h4>  
                                               <span id="component_content_<?php echo $index; ?>" class="componentName collapse"><?php the_content(); ?></span>
                                                
                                            </div>  
                                             
                                              
                                                <?php $index++; ?>
                                        </div>  
                             <?php } ?>                
                            <?php endwhile;?>
                               
                                <?php } ?>      
                        </div>
                           
                    </div>
                <?php } ?>
                    </div>
           
            </section>
        </div>
    </div>


  
    
   
<?php
return ob_get_clean();
}
add_shortcode( 'data_sheet_shortcode', 'data_sheet_shortcode' );

// share link data insert and update if id exit
function share_link_data() {
    global $wpdb;
    $share_link_id = $_POST['share_link_id'] ? $_POST['share_link_id']: uniqid() ;
    $_SESSION['share_link_id'] = $share_link_id;
    $component_json = $_POST['selected_addons'];
    $table_share = $wpdb->prefix . 'components_share_link';
       $component_json_clean = str_replace("\\", "", $component_json);
        $table_check_row = $wpdb->get_row("SELECT * FROM $table_share WHERE uuid = '$share_link_id'");
        if(empty($table_check_row)){
            $share_link_data =
            [
                'uuid' => $share_link_id,
                'share_link_data' => $component_json_clean,
            ];
            $wpdb->insert($table_share,$share_link_data);
        } else {
            $wpdb->update($table_share, ['share_link_data' => $component_json_clean], ['uuid' => $table_check_row->uuid]);
        }
    $response = array('type' => 'success', 'share_link_id' => $share_link_id, 'site_url' => get_site_url());
    echo json_encode($response);
    exit;
}
add_action('wp_ajax_share_link_data', 'share_link_data');
add_action('wp_ajax_nopriv_share_link_data', 'share_link_data');

add_action('wp_head', 'show_share_link_data');
// get data from db and show link for sharing in modal popup
function show_share_link_data() {
    global $wpdb;
    $table_share = $wpdb->prefix . 'components_share_link';
    $components_object = 'var components_object = {};';
    $share_link_id = @$_GET['share_link_id'];
   if (isset($share_link_id) && !empty($share_link_id)) {
        $addons_share_data = $wpdb->get_row("SELECT * FROM $table_share WHERE uuid = '$share_link_id'");
        // get first component id
        if(!empty($addons_share_data)) {
            foreach(json_decode($addons_share_data->share_link_data) as $selected_component) {
                $store_component_id = array_keys((array)$selected_component)[0]; 
            }
            $components_object = 'var components_object = {"components_data":' . $addons_share_data->share_link_data . ', "share_link_id":"' . $addons_share_data->uuid . '", "selected_component":' . $store_component_id . '};';
        }
    }
    echo "<script type='text/javascript'>\n";
    echo "/* <![SDATA[ */\n";
    echo $components_object;
    echo "\n/* ]]> */\n";
    echo "</script>\n";
}