<?php
if (isset($_POST['comp_id'])) {
    $cat_id = $_POST['comp_id'];
}
?>
<?php $components = get_configutor_child_texonomys($cat_id); ?>
<?php
// check addon exit in local storage or not
if(isset($_POST['relation']) && !empty($_POST['relation'])) {
    $reltion_arr = str_replace("\\", "", $_POST['relation']);
    $reltion_arr = json_decode($reltion_arr, true);
    $current_config_rel = $reltion_arr[$_POST['selected_component']];
    $product_ids = [];
    $title = '';
      
   
    $seleced_comp = $current_config_rel; 
    
    foreach($seleced_comp as $key => $arr_addon_rel) {
           $product_ids[$key] = $arr_addon_rel['addon'];
    }
    $selected_child_comp = array_keys($seleced_comp);
}
?>
<?php if (!empty($components)): ?>
    <div class="config-list">
        <?php foreach ($components as $key => $component): ?>
            <!-- get the addons title related to their component term-->
           <?php $title =  get_the_title( $product_ids[$component->term_id] ); ?>
            <?php

                if (isset($pre_selected_kits['component']) && in_array($component->term_id, $pre_selected_kits['component'])) {
                    $configClass = 'green-highlight';
                } elseif (isset($resp[0][0]->id)) {
                    if (!empty($resp[0][0]->compulsory_component)) {
                        $compulsory = explode(",", $resp[0][0]->compulsory_component);
                        if (in_array($component->term_id, $compulsory)) {
                            $configClass = 'red-highlight';
                        } else {
                            $configClass = 'gray-highlight';
                        }
                    } elseif (!empty($resp[0][0]->optional_component)) {
                        $optional = explode(",", $resp[0][0]->optional_component);
                        if (in_array($component->term_id, $optional)) {
                            $configClass = 'gray-highlight';
                        } else {
                            $configClass = 'red-highlight';
                        }
                    } else {
                        $configClass = 'gray-highlight';
                    }
                } else {
                    $validationType = get_term_meta($component->term_id, 'config-taxonomy-validation', true);
                    if ($validationType == 'yes') {
                        $configClass = 'red-highlight';
                    } elseif ($validationType == 'no' || $validationType == '') {
                        $configClass = 'gray-highlight';
                    }
                }
                if(!empty($selected_child_comp)) {
                    if(in_array($component->term_id, $selected_child_comp)) {
                        $configClass = 'green-highlight';
                    }
                }
                ?>
                <a href="javascript:void(0);" data-comp="<?php echo $component->term_id; ?>" class="config-overview-d <?php echo $configClass; ?>">
                    <div class="text">
                        <h5><?php echo $component->name; ?> :</h5>
                        <small><?php echo $title; ?></small>
                    </div>
                </a>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <p class="align-center">No record found!</p>
<?php endif; ?>