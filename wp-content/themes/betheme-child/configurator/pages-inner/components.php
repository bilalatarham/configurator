<?php
if(isset($_POST['configurator_id'])) {
    $cat_id = $_POST['configurator_id'];
}
$list_component = '';
?>
<?php $components = get_configutor_child_texonomys($cat_id); ?>

<?php
    $pre_selected_kits = [];
    if(isset($_POST['config_link_id']) && !empty($_POST['config_link_id']))
    {
        $pre_selected_kits = pre_selected_kits($_POST['config_link_id']);
    }
?>
<?php 
// check addon from local storage exit or not
if(isset($_POST['relation']) && !empty($_POST['relation'])) {
    $reltion_arr = str_replace("\\", "", $_POST['relation']);
    $reltion_arr = json_decode($reltion_arr, true);
    $current_config_rel = $reltion_arr;
    $first_sele_arr = array_keys($current_config_rel[$_POST['configurator_id']])[0];
    $term = get_term($first_sele_arr, 'configurator-category');
    $term_parent = ($term->parent == 0) ? $term : get_term($term->parent, 'configurator-category');
    $term_parent_id = $term_parent->term_id;

}
?>
<?php if(!empty($components)):?>
    <div class="sub-cat-list">
        <!-- html for preloader -->
        <div class="pre-loaderTab" >
            <img  src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/loader.gif"/>
        </div>
        <?php foreach($components as $key => $component): ?>
            <?php $image_id = get_term_meta( $component->term_id, 'configurator-taxonomy-image-id', true ); ?>
            <?php
                $check_child_comp = get_configutor_child_texonomys($component->term_id);
                $class = 'component-filter-box';
                if($check_child_comp) {
                    $class = 'show-child-comp-d';
                }
                if(isset($pre_selected_kits['component']) && in_array($component->term_id,$pre_selected_kits['component']) ) {
                    $class .= ' high-green ';
                }
                // if not empty
                if(!empty($term_parent_id)) {
                    // check child parent with component term for add class
                    if($term_parent_id == $component->term_id) {
                      $class .= ' high-green ';
                    }
                }
            ?>
            <a href="javascript:void(0)" comp-id="<?php echo $component->term_id;?>" class="list-item <?php echo $class;?>">
                <div class="icon">
                    <?php
                        $url = wp_get_attachment_image_url($image_id);
                        if ($url && @getimagesize($url)) {
                            $image = wp_get_attachment_image_url($image_id);
                        } else {
                            $image = get_stylesheet_directory_uri()."/assets/images/no-image.png";
                        }
                    ?>
                    <img class="scale-with-grid" src="<?php echo $image;?>">
                </div>
                <h5><?php echo $component->name; ?></h5>
            </a>
            <?php $list_component .= $component->term_id.','; ?>
        <?php endforeach; ?>
        <?php $list_component = rtrim($list_component,","); ?>
    </div>


    <!-- add parernt attribute in child upper div -->
    <div class="sub-cat-list" id="component-children-div-d" parent-comp="" style="display:none;border-top:1px solid;"></div>

<?php else: ?>
    <p class="align-center">No record found!</p>
<?php endif;?>
<input type="hidden" name="list_component" value="<?php echo $list_component;?>" />