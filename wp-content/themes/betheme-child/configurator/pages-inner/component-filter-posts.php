<?php

$term_post_tags = get_term_meta($_POST['comp_id'], 'post_title_tags', true);
// check store tag array empty then get title tag using comp_id when click at any filter
if(empty($store_tag_colors)) {
    if(!empty($term_post_tags)) {
        $term_post_arr = json_decode($term_post_tags);

        $i = 0;
        foreach($term_post_arr as $tag_post) {
            $color = filter_colors($i);
            $i++;

            if (!empty($tag_post->tags)) {
                foreach($tag_post->tags as $tag) {
                    $store_tag_colors[$tag]['background'] = $color['background'];
                    $store_tag_colors[$tag]['color'] = $color['color'];
                } 
            }
        }
    }
}
$add_ons_lists = get_config_posts_by_comp_id($_POST['comp_id'], $_POST);
$component_name = get_term($_POST['comp_id']);
?>
<?php if (!empty($add_ons_lists)): ?>
    <!-- Html for unselected addon -->
    <tr>
        <td class="pro-img">
            <?php 
            $image = get_stylesheet_directory_uri() . "/assets/images/remove.png";
            ?><img class="scale-with-grid" src="<?php echo $image; ?>" />
        </td>
        <td class="pro-img"></td>
        <td class="pro-radio-button"><input  class="unselect-addon" type="radio" name="addon" value="<?php echo $_POST['comp_id']; ?>">
        </td>
            <td class="pro-radio-button" colspan="5"> &nbsp; No <?php echo $component_name->name; ?></td>
    </tr>
        <?php
            $i = 0;
            $pre_selected_addons = '';
            if (isset($_POST['config_link_id']) && !empty($_POST['config_link_id'])) {
                $pre_addons = pre_selected_kits($_POST['config_link_id'], 'addon');
                if (isset($pre_addons['addon'][$_POST['comp_id']][0]) && !empty($pre_addons['addon'][$_POST['comp_id']][0])) {
                    $pre_selected_addons = $pre_addons['addon'][$_POST['comp_id']][0];
                }
            }
            foreach ($add_ons_lists['posts'] as $addon):
                $post_tag_cond = get_post_meta($addon->ID, 'post_title_tags', true);
                if (!empty($post_tag_cond) && !empty($add_ons_lists['searchrelation'])) {
                    $flag = 0;
                    foreach (explode(",", $post_tag_cond) as $validate) {
                        if (in_array($validate, $add_ons_lists['searchrelation'])) {
                            //goto move_last;
                            $flag = 1;
                            break;
                        }
                    }
                    if(!$flag)
                    {
                        goto move_last;
                    }
                } elseif (!empty($add_ons_lists['searchrelation'])) {
                    goto move_last;
                }
            $chil_comp = get_post_meta($addon->ID,"component_child",true);
        ?>
        <tr>
            <td class="pro-img">
                <?php
                    $url = wp_get_attachment_url(get_post_thumbnail_id($addon->ID));
                    if ($url && @getimagesize($url)) {
                        $image = wp_get_attachment_url(get_post_thumbnail_id($addon->ID), array(40, 12));
                    } else {
                        $image = get_stylesheet_directory_uri() . "/assets/images/no-image.png";
                    }
                ?>
                <img class="scale-with-grid" src="<?php echo $image; ?>" />
            </td>
            <td class="pro-title"><p><?php echo $addon->addon_sku; ?></p></td>
            <td class="pro-radio-button">
            <input data-price="<?php echo get_post_meta($addon->ID, 'addon_price', true); ?>" data-addon="<?php echo $addon->ID; ?>" data-comp="<?php echo $_POST['comp_id']; ?>" data-child-comp="<?php echo $chil_comp;?>" data-config="<?php echo $_POST['selected_configurator']; ?>" class="addon-d" type="radio" name="addon" value="<?php echo $addon->ID; ?>" <?php echo $pre_selected_addons == $addon->ID ? 'checked' : ''; ?> ></td>
            <td class="pro-title"><p><?php echo $addon->post_title; ?></p></td>
            <td>
                <?php
                    $post_filters = get_post_meta($addon->ID, 'post_title_tags', true);
                    if (!empty($post_filters)) {
                        $post_filters = explode(',', $post_filters);
                        foreach ($post_filters as $filters) {
                            $color = filter_colors($i);
                            $get_filter = get_tags_by_id($filters);
                            ?>
                            <!-- get color according to their group defined in store tag color array -->
                            <div class="st-label" style="background: <?php echo $store_tag_colors[$get_filter[0]->id]['background']; ?>; color: <?php echo $store_tag_colors[$get_filter[0]->id]['color']; ?>"><?php echo $get_filter[0]->text; ?></div>
                                    <?php
                    }
                        $i++;
                    }
                ?>
            </td>
            <td>
                <input type="number" min="1" class="quantity-d" name="" placeholder="Quantity e.g 1" value="1" />
            </td>
            <td>
                <?php
                    $addon_sale_price = get_post_meta($addon->ID, 'addon_actual_price', true);
                    if($addon_sale_price) {
                        $formatter = new NumberFormatter('en_GB', NumberFormatter::CURRENCY);
                        echo '<strike>'.$formatter->formatCurrency($addon_sale_price, 'EUR'), PHP_EOL.'</strike>';
                    }
                    $formatter = new NumberFormatter('en_GB', NumberFormatter::CURRENCY);
                    echo $formatter->formatCurrency(get_post_meta($addon->ID, 'addon_price', true), 'EUR'), PHP_EOL;
                ?>
            </td>
            <td>
                <div class="popup-info-slide">
                    <div class="slide-icon">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/info.svg" alt="-" class="scale-with-grid" width="25px">
                    </div>
                    <div class="popup-slide">
                        <?php echo $addon->post_content; ?>
                    </div>
                </div>
            </td>
        </tr>
        <?php move_last:'' ?>
    <?php endforeach; ?>
<?php else: ?>
    <tr>
        <td colspan="6" class="align-center">No records found!</td>
    </tr>
<?php endif; ?>