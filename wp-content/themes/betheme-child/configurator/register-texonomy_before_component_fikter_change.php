<?php

// register custom post type for configurator
add_action( 'init', 'register_configurator_post_type', 0);
function register_configurator_post_type() 
{
    $labels = [
		'name'                => _x( 'Configurators', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Configurator', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Configurators', 'text_domain' ),
        'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
        'all_items'           => __( 'All Add-ons', 'text_domain' ),
        'view_item'           => __( 'View Add-on', 'text_domain' ),
        'add_new_item'        => __( 'Add New Add-on', 'text_domain' ),
        'add_new'             => __( 'Add New Add-on', 'text_domain' ),
        'edit_item'           => __( 'Edit Add-on', 'text_domain' ),
        'update_item'         => __( 'Update Add-on', 'text_domain' ),
        'search_items'        => __( 'Search Add-on', 'text_domain' ),
        'not_found'           => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	];

    $args = [
        'label'               => __( 'Configurator', 'text_domain' ),
        'description'         => __( 'Create Configurator', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'editor', 'thumbnail', 'revisions','excerpt'],
        'taxonomies'          => ['configurator-category', 'configurator-category' ],
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-laptop',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',//'page'
	];
	register_post_type( 'configurator', $args );
}

// register custom texonomy for configurator post type
add_action( 'init', 'register_configurator_taxonomy' );
function register_configurator_taxonomy() 
{
    register_taxonomy(
        'configurator-category',
        'configurator',
        [
            'label' => __( 'Configurator Category' ),
            'rewrite' => array( 'slug' => 'configurator-category' ),
            'hierarchical' => true,
        ]
    );

    register_taxonomy(
        'configurator-manufacturer',
        'configurator',
        [
            'label' => __( 'Configurator Manufacturer' ),
            'rewrite' => array( 'slug' => 'configurator-manufacturer' ),
            'hierarchical' => true,
        ]
    );

// Add a taxonomy like tags
// $labels = array(
//     'name'                       => 'Attributes',
//     'singular_name'              => 'Attribute',
//     'search_items'               => 'Attributes',
//     'popular_items'              => 'Popular Attributes',
//     'all_items'                  => 'All Attributes',
//     'parent_item'                => null,
//     'parent_item_colon'          => null,
//     'edit_item'                  => 'Edit Attribute',
//     'update_item'                => 'Update Attribute',
//     'add_new_item'               => 'Add New Attribute',
//     'new_item_name'              => 'New Attribute Name',
//     'separate_items_with_commas' => 'Separate Attributes with commas',
//     'add_or_remove_items'        => 'Add or remove Attributes',
//     'choose_from_most_used'      => 'Choose from most used Attributes',
//     'not_found'                  => 'No Attributes found',
//     'menu_name'                  => 'Attributes',
//   );

//   $args = array(
//     'hierarchical'          => false,
//     'labels'                => $labels,
//     'show_ui'               => true,
//     'show_admin_column'     => true,
//     'update_count_callback' => '_update_post_term_count',
//     'query_var'             => true,
//     'rewrite'               => array( 'slug' => 'attribute' ),
//   );
//   register_taxonomy('configurator_tags','configurator',$args);

}

//get custom texonomy of configurator
function get_configutor_texonomys()
{
    $texonomy = get_terms(
        [
            'taxonomy'   => 'configurator-category',
            'hide_empty' => false,
            'parent'     => 0
        ]
    );

    return $texonomy;
}
//get custom chile texonomy of configurator
function get_configutor_child_texonomys($id)
{
    $child_texonomy = get_terms(
        [
            'taxonomy'   => 'configurator-category',
            'hide_empty' => false,
            'parent'     => $id
        ]
    );

    return $child_texonomy;
}

//get configurator and componenets fro frot-end side
add_action('wp_ajax_get_configurat_components', 'get_configurat_components');
add_action('wp_ajax_nopriv_get_configurat_components', 'get_configurat_components');
function get_configurat_components()
{
    if($_POST['configurator_type'] == 'component')
    {
        include('pages-inner/components.php');
    }
    else if($_POST['configurator_type'] == 'config_overview')
    {
        include('pages-inner/configurator-overview.php');
    }
    exit();
}



//create meta boxes for variants
add_action( 'add_meta_boxes', 'config_create_variant_metabox'  );
/* Do something with the data entered */
add_action( 'save_post',  'config_save_variants_data'  );


/* Adds a box to the main column on the Post and Page edit screens */     
function config_create_variant_metabox() 
{
    add_meta_box(
        'dynamic_sectionid',
        __( 'Filters and Component Relation', 'myplugin_textdomain' ),
        'config_create_variants_fields',
        'configurator',
        'normal',
        'high'
    );
}

  /* Prints the metabox content */
function config_create_variants_fields() {
    global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'config_variant_nonce' );
    ?>
    <div id="meta_inner">
        <?php
        
            //get the saved meta as an arry
            // $albums = get_post_meta( $post->ID, 'album', true );
            // $c = 0;
            // if ( count( $albums ) > 0 ) {
            //     foreach( $albums as $album ) {
            //         if ( isset( $album['title'] ) || isset( $album['track'] ) ) {
            //             printf( '<p>Heading: <input type="text" name="album[%1$s][title]" value="%2$s" /> ', $c, $album['title'], __( 'Remove Album' ) );
            //             $c = $c +1;
            //         }
            //     }
            // }
        ?>
        <input type="hidden" name="curr_post_id" id="curr_post_id" value="<?php echo $post->ID; ?>" />
        <p class="form-field">
            <label for="configurator_id"><?php _e( 'Configurator Types' ); ?></label>
            <select class="" style="width:50%" id="configurator_id" name="configurator_id">
                <?php $conf_id = get_post_meta( $post->ID, 'configurator_id', true ); ?>
                <?php $configurators = get_configutor_texonomys(); ?>
                <?php if(!empty($configurators)): ?>
                    <?php foreach($configurators as $key => $configrator): ?>
                        <?php
                            $selected = '';
                            if( $conf_id) {
                                if($configrator->term_id == $conf_id) {
                                    $selected = 'selected';
                                }
                            } else if($key == 0) {
                                $selected = 'selected';
                            }
                        ?>
                        <option value="<?php echo $configrator->term_id;?>" <?php if($conf_id)?> <?php echo $selected; ?>><?php echo $configrator->name; ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </p>
        <p class="form-field">
            <label for="configurator_component"><?php _e( 'Components' ); ?></label>
            <select class="" style="width:50%" id="configurator_component" name="configurator_component">
                <option value=""></option>
            </select>
            <a href="javascript:void(0)" class="show-relation-block">Link Components</a>
        </p>

        <div class="filter-relation-block-d" style="display: none;">
                <p style="border-top:1px solid;"></p>
                <div class="" style="width: 25%;float:left">
                    <p>
                        <label><?php _e( 'Component' ); ?></label>
                        <select style="width:50%" class="source_selected_component" name="" disabled>
                        </select>
                    </p>
                    <p>
                        <label><?php _e( 'Filters' ); ?></label>
                        <select style="width:50%" multiple="multiple" class="source_selected_tags" name="source_selected_tags[]">
                        </select>
                    </p>
                </div>
                <div class="filter-dest-relation" style="width: 73%;float:left;border-left:1px solid">
                    <p class="" style="width:50%;float: left;">
                        <label><?php //_e( 'Destination Relation' ); ?></label>
                        <select style="width:50%" class="component_relation_filter" name="post_relation[0][component][]">
                        </select>
                    </p>
                    <p class="" style="width:50%;float:left;">
                        <label><?php //_e( 'Filter Tags' ); ?></label>
                        <select multiple="multiple" style="width:50%" class="component_relation_tags" name="post_relation[0][tag][]">
                        </select>
                        <a class="add-filter-dest" href="javascript:void(0)">Add More</a>
                    </p>
                </div>
                <br>
                <a href="javascript:void(0)" class="hide-relation-block" style="display: none;float: right;margin-bottom: 16px;margin-right: 30px;">Hide</a>
        </div>
        <p style="border-bottom:1px solid;clear:both;"></p>
        <?php 
            $post_title_tags = get_post_meta($post->ID,'post_title_tags',true);
            $tags_counter = 1;
            if(!empty($post_title_tags))
            {
                $post_title_tags = json_decode($post_title_tags,true);
                //print_r($post_title_tags);
                $tags_counter = count($post_title_tags);
            }
        ?>
        <div style="width:100%">
            <input type="hidden" id="current_post_filter_tags" value="<?php echo $tags_counter; ?>" />
            <input type="hidden" id="component_id" name="component_id" value="<?php echo get_post_meta($post->ID,'component_id',true); ?>" />
            <!-- <p class="" style="width:45%;float:left;">
                <label style="width:20%"><?php //_e( 'Title' ); ?></label>
                <input style="width:50%" type="text" name="post_title_tags[0][title][]" value="" />
            </p>
            <p class="" style="width:45%;float:left;">
                <label><?php //_e( 'Filter tags' ); ?></label>
                <select multiple="multiple" style="width:50%" class="post_tags" name="post_title_tags[0][tags][]">
                    <option value=""></option>
                </select>
            </p>
            <p class="add-new-filter" style="width:10%;float:left;"><a href="javascript:void(0)">+ Add new</a></p> -->
            <div style="clear: both;height:5px"></div>
            <div class="select-container"></div>
        </div>

        <span id="here"></span>
        <!-- <span class="button addalbum"><?php //_e('Add Album'); ?></span>
        <span class="button addtracks"><?php //_e('Add Tracks'); ?></span> -->
        <script>
            var $ =jQuery.noConflict();
            $(document).ready(function() {
                
                //var count = <?php //echo $c; ?>;
                $(".addtracks").click(function() {
                    count = count + 1;
                    $('#here').append('<p> Track number : <input type="text" name="album['+count+'][track]" value="" /><span class="removetrack">Remove Track</span></p>' );
                    return false;
                });

                $(".removealbum").on('click', function() {
                    $(this).parent().remove();
                });
            });
        </script>
    </div>
    <?php
}

/* When the post is saved, saves our custom data */
function config_save_variants_data( $post_id ) {
    // verify if this is an auto save routine.
    // If it is our form has not been submitted, so we dont want to do anything
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !isset( $_POST['config_variant_nonce'] ) )
    {
        return;
    } 
    
    if ( !wp_verify_nonce( $_POST['config_variant_nonce'], plugin_basename( __FILE__ ) ) )
    {
        return;
    }

    // OK, we're authenticated: we need to find and save the data
    

    
    update_post_meta( $post_id, 'component_id', $_POST['component_id'] );
    update_post_meta( $post_id, 'source_selected_tags', wp_slash(json_encode($_POST['source_selected_tags'])) );
    update_post_meta( $post_id, 'post_relation', wp_slash(json_encode($_POST['post_relation'])) );
    update_post_meta( $post_id, 'configurator_id', $_POST['configurator_id'] );
    update_post_meta( $post_id, 'configurator_component', $_POST['configurator_component'] );
    
    if(isset($_POST['post_title_tags'][0]['title'][0]) && !empty($_POST['post_title_tags'][0]['title'][0]))
    {
        update_post_meta( $post_id, 'post_title_tags', wp_slash(json_encode($_POST['post_title_tags'])) );
    }
    else
    {
        update_post_meta( $post_id, 'post_title_tags', '' );
    }
}

//create meta boxes for add-on price
add_action( 'add_meta_boxes', 'config_create_price_metabox'  );
/* Do something with the data entered */
add_action( 'save_post',  'config_save_price_data'  );


/* Adds a box to the main column on the Post and Page edit screens */     
function config_create_price_metabox() 
{
    add_meta_box(
        'config_prices_id',
        __( 'Price', 'config_textdomain' ),
        'config_create_prices_fields',
        'configurator',
        'normal',
        'high'
    );
}

/* Prints the metabox content */
function config_create_prices_fields() {
    global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'config_price_nonce' );
    ?>
    <div id="meta_inner_price">
        <p class="form-field">
            <label><?php _e( 'Price' ); ?></label>
            <input type="text" name="addon_price" value="<?php echo get_post_meta($post->ID,'addon_price',true); ?>" />
        </p>
    </div>
    <?php
}

/* When the post is saved, saves our custom data */
function config_save_price_data( $post_id ) {
    // verify if this is an auto save routine.
    // If it is our form has not been submitted, so we dont want to do anything
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !isset( $_POST['config_price_nonce'] ) )
    {
        return;
    }

    if ( !wp_verify_nonce( $_POST['config_price_nonce'], plugin_basename( __FILE__ ) ) )
    {
        return;
    }

    // OK, we're authenticated: we need to find and save the data
    update_post_meta( $post_id, 'addon_price', $_POST['addon_price'] );
}

//get componenets for admin side side [configurator custom post type in post]
add_action('wp_ajax_get_admin_components', 'get_admin_components');
add_action('wp_ajax_nopriv_get_admin_components', 'get_admin_components');
function get_admin_components()
{
    global $wpdb;

    $id = $_POST['configurator_id'];
    $components = get_configutor_child_texonomys($id);
    $source_options = '';
    $dest_option = '';
    $source_selected_opt = '';
    $options['source_component_tags'] = '';
    $options['dest_option'] = '';
    $options = [];
    if(!empty($components))
    {
        $post_component = get_post_meta( $_POST['current_post_id'], 'configurator_component', true );

        foreach($components as $component)
        {
            $selected = '';
            if( $post_component )
            {
                if($component->term_id == $post_component)
                {
                    $selected = 'selected';
                }
            }
            $source_options .= '<option value="'.$component->term_id.'" '.$selected.' >'.$component->name.'</option>';

            if(isset($_POST['source_selected_id']) && $_POST['source_selected_id'] && $_POST['source_selected_id'] != $component->term_id)
            {
                //$dest_option .= '<option value="'.$component->term_id.'" '.$selected.' >'.$component->name.'</option>';

                $store['id']       = $component->term_id;
                $store['text']     = $component->name;
                $store['selected'] = isset($selected) && !empty($selected) ? true : false;
                $options['dest_option'][] = $store;
            }
            
            if(isset($_POST['source_selected_id']) && $_POST['source_selected_id'] && $_POST['source_selected_id'] == $component->term_id)
            {
                $source_selected_opt .= '<option selected value="'.$component->term_id.'" '.$selected.' >'.$component->name.'</option>';
            }
        }

        if(isset($_POST['source_selected_id']) && !empty($_POST['source_selected_id']))
        {
            $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
            WHERE post_id = ".$_POST['current_post_id']." and configurator_id = ".$_POST['configurator_id']." and component = ".$_POST['source_selected_id']." ");
            $results = (array) $tags;
            
            foreach($results as $res)
            {
                $store['id']       = $res->id;
                $store['text']     = $res->text;
                //$store['selected'] = $selected;
                $options['source_component_tags'][] = $store;
            }
        }

        if(isset($_POST['source_selected_id']) && !empty($_POST['source_selected_id']))
        {
            $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
            WHERE post_id = ".$_POST['current_post_id']." and configurator_id = ".$_POST['configurator_id']." and component = ".$_POST['source_selected_id']." ");
            $results = (array) $tags;
            
            
            $source_selected_tags =  get_post_meta($_POST['current_post_id'],'source_selected_tags',true);
            foreach($results as $res)
            {
                $selected = 0;
                if(!empty($source_selected_tags))
                {
                    $source_selected_tags = json_decode($source_selected_tags,true);
                    if(in_array($res->id,$source_selected_tags))
                    {
                        $selected = 1;
                    }
                }
                $store['id']       = $res->id;
                $store['text']     = $res->text;
                $store['selected'] = $selected;
                $options['source_component_tags'][] = $store;
            }
        }
    }

    $options['source_components'] = $source_options;
    $options['source_component_tags'] = $options['source_component_tags'];
   // $options['post_relation'] = $options['post_relation'];

    $options['source_selected_option'] = $source_selected_opt;
    $options['dest_components'] = $options['dest_option'];
    $options['dest_component_tags'] = $dest_option;
    // print_r($post_relation);
    // exit;
    $post_relation = get_post_meta($_POST['current_post_id'],'post_relation',true);
    if(!empty($post_relation ))
    {
        $post_relation = json_decode($post_relation,true);
       
        $create_comp_tags = [];
        foreach ($post_relation as $key => $value)
        {
            if(isset($value['component'][0]))
            {
                $store_post['component'] = $value['component'][0];
                if(!empty(get_tags_by_id($value['component'][0])))
                {
                    $store_post['tags'] = get_tags_by_id($value['component'][0]);
                }
                else
                {
                    $store_post['tags'] = [];
                }

                if(isset($value['tag']))
                {
                    $store_post['selected_tags'] = $value['tag'];
                }
                else
                {
                    $store_post['selected_tags'] = [];
                }
                $create_comp_tags[] = $store_post;
            }
        }
        $options['relation_component_tags'] = $create_comp_tags;
    }
    else
    {
        $options['relation_component_tags'] = false;
    }
    
    echo json_encode($options);
    exit();
}


//get component tags by id
function get_tags_by_id($id)
{

    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    WHERE component = ".$id." ");

    $results = (array)$tags;

    return $results;
}


//get componenets for admin side side [configurator custom post type in post]
add_action('wp_ajax_config_filter_tags', 'config_filter_tags');
add_action('wp_ajax_nopriv_config_filter_tags', 'config_filter_tags');
function config_filter_tags()
{
    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    WHERE post_id = ".$_POST['post_id']." and configurator_id = ".$_POST['configurator_id']." and component = ".$_POST['component']." and tag = '".$_POST['tag']."' ");
    if(empty($tags))
    {
        $selected_tag = [];
        $saveTags = 
        [
            'post_id'         => $_POST['post_id'],
            'configurator_id' => $_POST['configurator_id'],
            'component'       => $_POST['component'],
            'tag'             => $_POST['tag'],
        ];

        $wpdb->insert('wp_configurator_tags',$saveTags);
        $last_id = $wpdb->insert_id;
        foreach ($_POST['selected_post_tags'] as $value) 
        {       
            if(strpos($value,'(New)') !== false)
            {
                $selected_tag[] = $last_id;
            }
            else
            {
                $selected_tag[] = $value;
            }
        }
    
        $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
        WHERE post_id = ".$_POST['post_id']." and configurator_id = ".$_POST['configurator_id']." and component = ".$_POST['component']." ");
        $results = (array) $tags;
        $options = [];
       
        foreach($results as $res)
        {
            $selected = false;
            if(in_array($res->id,$selected_tag))
            {
                $selected = true;
            }
            $store['id']       = $res->id;
            $store['text']     = $res->text;
            $store['selected'] = $selected;
            $options['component_tags'][] = $store;
        }
        echo json_encode($options);
    }

    
    // $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    // WHERE id = ".$wpdb->insert_id." ");
    // $results = (array)$tags;
    //echo json_encode(['data' => $results]);
    exit();
}

//get componenets tags for admin side [configurator custom post type in post]
add_action('wp_ajax_get_component_tags', 'get_component_tags');
add_action('wp_ajax_nopriv_get_component_tags', 'get_component_tags');
function get_component_tags()
{
    global $wpdb;

    // $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    // WHERE post_id = ".$_POST['post_id']." and configurator_id = ".$_POST['configurator_id']." and component = ".$_POST['component']." ");

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    WHERE configurator_id = ".$_POST['configurator_id']." and component = ".$_POST['component']." ");

    $results = (array)$tags;

    $options = [];
    if(!empty($results))
    {
        foreach($results as $res)
        {
            $store['id']       = $res->id;
            $store['text']     = $res->text;
            $store['selected'] = false;
            $options['component_tags'][] = $store;
        }
    }
    
    //if(!empty($_POST['post_id']) && get_post_meta($_POST['post_id'],'component_id',true) == $_POST['component'])
    if(!empty(get_post_meta($_POST['post_id'],'component_id',true) == $_POST['component']))
    {
        $post_title_tags = get_post_meta($_POST['post_id'],'post_title_tags',true);
        if(!empty($post_title_tags))
        {
            $post_title_tags = json_decode($post_title_tags,true);
            foreach($post_title_tags as $key => $post_title)
            {
                $store['title']       = $post_title['title'][0];
                $store['tags_id']     = $post_title['tags'];
                $options['post_component_tags'][] = $store;
            }
        }
    }
    echo json_encode($options);
    exit();
}


//get componenets and tags with relationship for admin side [configurator custom post type in post]
add_action('wp_ajax_get_component_and_tags_with_relation', 'get_component_and_tags_with_relation');
add_action('wp_ajax_nopriv_get_component_and_tags_with_relation', 'get_component_and_tags_with_relation');
function get_component_and_tags_with_relation()
{
    global $wpdb;
    $components = get_configutor_child_texonomys($_POST['configurator_id']);

    $options = [];
    if(!empty($components))
    {
        foreach($components as $res)
        {
            if($_POST['component'] != $res->term_id){
                $store['id']       = $res->term_id;
                $store['text']     = $res->name;

                $options['relation_component'][] = $store;
            }
        }
    }
    echo json_encode($options);
    exit();
}


//get componenets and tags with relationship for admin side [configurator custom post type in post]
add_action('wp_ajax_get_tags_by_components', 'get_tags_by_components');
add_action('wp_ajax_nopriv_get_tags_by_components', 'get_tags_by_components');
function get_tags_by_components()
{
    global $wpdb;

    // $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    // WHERE post_id = ".$_POST['post_id']." and component = ".$_POST['component']." ");
    
    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    WHERE component = ".$_POST['component']." ");
    $results = (array)$tags;
  
    $options = [];
    if(!empty($results))
    {
        foreach($results as $res)
        {
            if($_POST['component'] != $res->id)
            {
                $store['id']       = $res->id;
                $store['text']     = $res->text;
                $options['relation_component_tags'][] = $store;
            }
        }
    }
    echo json_encode($options);
    exit();
}

//get componenets and tags with relationship for admin side [configurator custom post type in post]
add_action('wp_ajax_component_block', 'component_block');
add_action('wp_ajax_nopriv_component_block', 'component_block');
function component_block()
{
    include('pages-inner/component-filter.php');
    exit();
}

function get_config_posts_by_comp_id($id)
{
    $add_ons = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type' => 'configurator',
            'tax_query' => array(
                array(
                    'taxonomy' => 'configurator-category',
                    'field' => 'term_id',
                    'terms' => $id,
                )
            )
        )
    );
    return $add_ons;
}

function group_config_tags_manufacturer($id)
{
    $get_posts = get_config_posts_by_comp_id($id);
   
    $return = [];
    $store_filter = [];
    foreach($get_posts as $get_post)
    {
        $term = wp_get_post_terms( $get_post->ID, 'configurator-manufacturer' );
        if(!empty($term))
        {
            foreach($term as $mft)
            {
                $return['manufacturer'][$mft->term_id] = ['id' => $mft->term_id ,'text' => $mft->name];
            }
        }

        $post_title_tags = get_post_meta($get_post->ID,'post_title_tags',true);
        
        if(!empty($post_title_tags))
        {
            $post_title_tags = json_decode($post_title_tags,true);
            foreach($post_title_tags as $filter_data)
            {
                if(!empty($filter_data['tags']))
                {
                    $filter['title'] = $filter_data['title'][0];
                    $filter['tag'] = get_tags_by_comp_in_ids($filter_data['tags']);
                    $return['tags'][] = $filter;
                }
            }
        }
    }
    return $return;
}

function get_tags_by_comp_in_ids($ids)
{
    global $wpdb;

    $tags = $wpdb->get_results("SELECT id,tag as text FROM wp_configurator_tags 
    WHERE id IN(".implode(',',$ids).") ");
    $results = (array)$tags;

    return $results;
}